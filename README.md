# LADON
> Matthew Kline, Andrew Sagerian, Jack Warren, Garrett Tucker

A proof-of-concept dynamic multo-channel C2 system, with TCP, UDP, and Discord API as channels.

More information is available in the project report.

## Installation

```bash
pip install -r requirements.txt
```

You *must* run the above. This project incorporates code across both Go and Python, and Python IDEs will trip if they try to handle dependencies on their own.

## Running

```bash
python -m ladon.entrypoint -h
```

Help text will walk you through constructing a proper command.

### Examples

Running the server with `tcp` as the only channel:

```bash
python -m ladon.entrypoint server --protocols 'tcp'
```

The output of the server command will inform you of connection information if you do not specify it. For example, if you ran the above, the server might pick `61727` as the port, which means you could run:

```bash
python -m ladon.entrypoint client --protocols 'tcp localhost 61727'
```

Multiple protocols can be provided by comma separation in the single-quoted string.

## Usage

Once the server is online, an API will be available from http://localhost:8080/api/v1/doc.

The API provides status information for troubleshooting purposes along with access to connected clients to simulate file exfiltration, remote shell usage, etc.
