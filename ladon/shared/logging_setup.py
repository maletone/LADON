import logging
import datetime

from typing import Optional, Union

TRACE_LEVEL = logging.DEBUG - 5

#
# TO CHANGE DEFAULT LOG LEVEL:
#   Change the value below. Can set to any constant in `logging` or to `TRACE_LEVEL`.
#

LEVEL = logging.INFO


#
# TO CONFIGURE LOGGING IN A FILE:
#   Call this function and call log methods on the result.
#       >>> log = get_logger('my module')
#       >>> log.info('foo bar')
#       '... foo bar ...'
#
#   You can change the level if you'd like file-level granularity
#       >>> log = get_logger('my broken module', 'TRACE')
#       >>> log.trace('foo bar')
#       '... foo bar ...'
#   Note in the above example that `trace` is a custom level below `debug`.
#

def get_logger(name: str, level: Union[int, str] = LEVEL) -> logging.Logger:
    logger = logging.getLogger(name)
    logger.setLevel(level)
    return logger


#
# Basement
#


# Function grabbed from Stack Overflow at https://stackoverflow.com/a/35804945
def __add_logging_level(level_name: str, level_num: int, method_name: Optional[str] = None):
    """
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

    `levelName` becomes an attribute of the `logging` module with the value
    `levelNum`. `methodName` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
    used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present

    Example
    -------
    >>> __add_logging_level('TRACE', logging.DEBUG - 5)
    >>> logging.getLogger(__name__).setLevel("TRACE")
    >>> logging.getLogger(__name__).trace('that worked')
    >>> logging.trace('so did this')
    >>> logging.TRACE
    5

    """
    if not method_name:
        method_name = level_name.lower()

    if hasattr(logging, level_name):
        raise AttributeError('{} already defined in logging module'.format(level_name))
    if hasattr(logging, method_name):
        raise AttributeError('{} already defined in logging module'.format(method_name))
    if hasattr(logging.getLoggerClass(), method_name):
        raise AttributeError('{} already defined in logger class'.format(method_name))

    def log_for_level(self, message, *args, **kwargs):
        if self.isEnabledFor(level_num):
            self._log(level_num, message, args, **kwargs)

    def log_to_root(message, *args, **kwargs):
        logging.log(level_num, message, *args, **kwargs)

    logging.addLevelName(level_num, level_name)
    setattr(logging, level_name, level_num)
    setattr(logging.getLoggerClass(), method_name, log_for_level)
    setattr(logging, method_name, log_to_root)


timestamp = str(datetime.datetime.now()).replace(" ", "_").replace(":", ";")
logging.basicConfig(level=LEVEL,
                    format="%(asctime)s:  ||  %(name)-20s ||  %(levelname)-8s ||  %(message)-50s ||  %(filename)s:%(lineno)d")

__add_logging_level("TRACE", TRACE_LEVEL)
