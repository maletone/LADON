import abc
import asyncio
from logging import Logger
from pathlib import Path
from typing import Iterable, Union

from ladon.shared.logging_setup import get_logger, LEVEL


class AbstractController(abc.ABC):
    log: Logger
    temp: Path
    recv_count: int = 0

    def __init__(self, temp: Path, log_level: int = LEVEL):
        self.temp = temp
        self.log = get_logger(self.__class__.__name__, log_level)

    @abc.abstractmethod
    async def get_tasks(self) -> Iterable[asyncio.Task]:
        # Don't dare await anything here! Just create tasks for async methods that need to run forever and return them.
        # Stuff like itertools.chain will calm down the linter, see client.py.
        ...

    async def start(self):
        self.log.info("Starting {0}".format(self.__class__.__name__))
        tasks = await self.get_tasks()
        done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)

        def future_name(fut: Union[asyncio.Task, asyncio.Future]) -> str:
            return fut.get_name() if isinstance(fut, asyncio.Task) else "UNKNOWN FUTURE: {0}".format(str(fut))

        for future in done:
            name = future_name(future)
            try:
                result = future.result()
                self.log.fatal("{0} task {1} exited unexpectedly with {2}".format(
                    self.__class__.__name__, name, result
                ))
            except asyncio.CancelledError as e:
                self.log.fatal("{0} task {1} was cancelled".format(
                    self.__class__.__name__, name
                ), exc_info=e)
            except Exception as e:
                self.log.fatal("{0} task {1} raised an exception".format(
                    self.__class__.__name__, name
                ), exc_info=e)
        self.log.debug("{0} has {1} tasks still pending: {2}".format(
            self.__class__.__name__, pending, ", ".join(map(future_name, pending))
        ))
