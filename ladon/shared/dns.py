from typing import TypeVar, Union

T = TypeVar('T')


def hostname_str(host: T) -> Union[T, str]:
    """
    Unfortunately, DNS has platform-dependent behavior. We could ignore this if
    it weren't for glaring oddities like `localhost` taking multiple seconds
    to resolve on Windows.
    (https://stackoverflow.com/questions/1981778/accepting-a-socket-on-windows-7-takes-more-than-a-second)

    This function can be used as an argparse "type" to transform given hostnames
    immediately upon input.

    :param host: some input representing the hostname
    :return: the hostname, possibly after some transformation
    """
    if host == 'localhost':
        return '127.0.0.1'
    else:
        return host
