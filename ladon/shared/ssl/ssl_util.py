import os
import ssl
from os import path


def get_no_verify_context() -> ssl.SSLContext:
    params = next(
        filter(
            lambda p: path.exists(p),
            map(
                lambda p: path.join(path.dirname(__file__), p),
                ["dhparams.pem",
                 "ladon/shared/ssl/dhparams.pem",
                 "../../shared/ssl/dhparams.pem"
                 "../../../shared/ssl/dhparams.pem",
                 "../../../../shared/ssl/dhparams.pem"]
            )
        )
    )
    context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_2)
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    context.set_ciphers("ADH-AES256-GCM-SHA384@SECLEVEL=0")
    context.load_dh_params(params)
    return context
