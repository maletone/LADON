package ladon

import (
	"github.com/kbinani/screenshot"
	"image/png"
	"os"
)

func TakeScreenshot(file_name string) {
	n := screenshot.NumActiveDisplays()
	for i := 0; i < n; i++ {
		bounds := screenshot.GetDisplayBounds(i)
		img, err := screenshot.CaptureRect(bounds)
		if err != nil {
			panic(err)
		}
		file, _ := os.Create(file_name + ".png")
		defer file.Close()
		png.Encode(file, img)
	}
}