import functools
import operator
import os
from datetime import datetime, timedelta
from typing import Final, List, Optional, Dict

from ladon.shared.interchange_pb2 import LadonMessage, MessagePart, PartRetryRequest, LadonAtom
from ladon.shared.util import get_or_none

from ladon.shared.logging_setup import get_logger

log = get_logger('Mailbox')


class OutgoingAtomProducer:
    series: bytes
    data: bytes
    produced_atoms: List[MessagePart]
    produced_data_length: int
    last_used: Optional[datetime]
    done: bool

    def __init__(self, message: LadonMessage):
        self.series = Mailbox.make_series()
        self.data = message.SerializeToString()
        self.produced_atoms = []
        self.produced_data_length = 0
        self.last_used = None
        self.done = False

    def produce_next_atom(self, size: Optional[int]) -> Optional[MessagePart]:
        if self.produced_data_length < len(self.data):
            if size is None or self.produced_data_length + size > len(self.data):
                target_end = len(self.data)
            else:
                target_end = self.produced_data_length + size
            assert target_end <= len(self.data)

            part_data = self.data[self.produced_data_length:target_end]
            self.produced_data_length += len(part_data)

            self.last_used = datetime.now()

            part = MessagePart()
            part.series = self.series
            part.part_offset = len(self.produced_atoms)
            if self.produced_data_length >= len(self.data):
                part.last = True
                self.done = True
            else:
                part.last = False
            part.data = part_data
            self.produced_atoms.append(part)
            return part

    def get_sent_atom(self, part_offset: int) -> MessagePart:
        ret = get_or_none(self.produced_atoms, part_offset)
        if ret is None:
            raise RuntimeError('Not sent atom ' + str(part_offset) + ' for series ' + str(self.series))
        return ret

    def is_stale(self, delta: timedelta) -> bool:
        return self.last_used + delta < datetime.now()


class IncomingAtomConsumer:
    series: str
    received_parts: List[bytes]
    expected_part_count: Optional[int]
    last_used: Optional[datetime]
    done: bool = False

    def __init__(self, series: str):
        self.series = series
        self.received_parts = []
        self.expected_part_count = None
        self.last_used = None
        self.done = False

    def _store_data_at(self, idx: int, data: bytes) -> None:
        try:
            self.received_parts[idx] = data
        except IndexError:
            for _ in range(idx + 1 - len(self.received_parts)):
                # Python doesn't have flowtyping so an actual List[Optional[bytes]] would be awful
                # to work with. Just ignore the complaint about adding None to List[bytes]
                # noinspection PyTypeChecker
                self.received_parts.append(None)
            self.received_parts[idx] = data

    def _is_done(self) -> bool:
        if self.expected_part_count is None:
            return False
        for i in range(0, self.expected_part_count):
            try:
                if self.received_parts[i] is None:
                    return False
            except IndexError:
                return False
        return True

    def consume_atom(self, part: MessagePart) -> Optional[LadonMessage]:
        if self.series != part.series:
            raise RuntimeError('Series mismatch')
        if get_or_none(self.received_parts, part.part_offset) is None:
            self.last_used = datetime.now()
            self._store_data_at(part.part_offset, part.data)
            if part.last:
                self.expected_part_count = part.part_offset + 1
            if self._is_done():
                self.done = True
                ret = LadonMessage()
                ret.ParseFromString(b''.join(self.received_parts))
                return ret

    def derive_necessary_retries(self) -> List[PartRetryRequest]:
        def make_retry(offset) -> PartRetryRequest:
            retry = PartRetryRequest()
            retry.series = self.series
            retry.part_offset = offset
            return retry

        return [make_retry(offset) for offset
                in range(self.expected_part_count or (len(self.received_parts) + 1))
                if get_or_none(self.received_parts, offset) is None]

    def is_stale(self, delta: timedelta) -> bool:
        return self.last_used + delta < datetime.now()


class Mailbox:
    SERIES_BYTE_LENGTH: Final = 2

    @staticmethod
    def make_series() -> bytes:
        return os.urandom(Mailbox.SERIES_BYTE_LENGTH)

    @staticmethod
    @functools.cache
    def message_part_overhead() -> int:
        atom = LadonAtom()
        atom.message_part.series = Mailbox.make_series()
        # Other non-data fields use default values and variable-length encoding. All
        # we can do is guess...
        # TODO?: well, the above isn't entirely true. We could theoretically know
        #   the value of the other fields inside the produce_next_atom method. If we
        #   need a more exact atom size generator, we'd change that method to accept
        #   the size of the entire atom and get rid of this method and just do the
        #   calculation there. Would sorta have to be brute-force/iterative to get it
        #   exact still.
        return len(atom.SerializeToString()) + 4

    # Message series correlated to a producer of atoms for that series
    outgoing: Dict[bytes, OutgoingAtomProducer]

    # An effectively unordered set of atoms that need to go out
    #   Basically gets used for queuing retries. We always look here for
    #   atoms to send and if we don't find any then we generate one, so this
    #   just lets us prioritize retries.
    outgoing_atoms: List[LadonAtom]

    # Message series correlated to a consumer of atoms (builder of messages) for that series
    incoming: Dict[bytes, IncomingAtomConsumer]

    def __init__(self):
        self.outgoing = {}
        self.outgoing_atoms = []
        self.incoming = {}

    # Accepts a part, consumes it, possibly produces a message
    def handle_part(self, part: MessagePart) -> Optional[LadonMessage]:
        if part.series not in self.incoming:
            self.incoming[part.series] = IncomingAtomConsumer(part.series)
        return self.incoming[part.series].consume_atom(part)

    # Accepts a retry request, adds the requested part to the outgoing queue
    def handle_retry(self, retry: PartRetryRequest) -> None:
        if retry.series not in self.outgoing:
            raise RuntimeError('Retry for unknown outgoing message')
        atom = LadonAtom()
        atom.message_part.CopyFrom(self.outgoing[retry.series].get_sent_atom(retry.part_offset))
        self.outgoing_atoms.append(atom)

    # Accepts a message, adds a producer of atoms for it, return the series
    def prepare_message_for_sending(self, message: LadonMessage) -> bytes:
        producer = OutgoingAtomProducer(message)
        self.outgoing[producer.series] = producer
        return producer.series

    # Accepts a desired maximum size, returns an atom if there's one to send
    def get_next_to_send(self, size: Optional[int]) -> Optional[LadonAtom]:
        log.trace("get_next_to_send called: size {0}".format(size))
        atom = None
        for a in self.outgoing_atoms:
            if size is None or len(a.SerializeToString() <= size):
                atom = a
                break
        if atom is not None:
            self.outgoing_atoms.remove(atom)
        else:
            if size is not None and size - Mailbox.message_part_overhead() <= 0:
                log.fatal("get_next_to_send size {0} too small for overhead {1}".format(
                    size, Mailbox.message_part_overhead()
                ))
                return
            target_part_size = size - Mailbox.message_part_overhead() if size is not None else None
            for producer in filter(lambda p: not p.done, self.outgoing.values()):
                part = producer.produce_next_atom(target_part_size)
                if part is not None:
                    atom = LadonAtom()
                    atom.message_part.CopyFrom(part)
        return atom

    # Adds necessary retry requests to the outgoing queue
    #   If a delta is given, filter to only stale consumers rather than all
    def queue_necessary_retries(self, incoming_delta: Optional[timedelta] = None) -> None:
        def form_atom(retry: PartRetryRequest) -> LadonAtom:
            atom = LadonAtom()
            atom.part_retry_request.CopyFrom(retry)
            return atom

        consumers = self.incoming.values()
        if incoming_delta is not None:
            consumers = filter(lambda c: c.is_stale(incoming_delta), consumers)
        self.outgoing_atoms.extend(
            map(form_atom,
                functools.reduce(operator.iconcat, map(lambda c: c.derive_necessary_retries(), consumers), []))
        )

    # Delete stale consumers/producers based on deltas
    def prune_stale(self, outgoing_delta: timedelta, incoming_delta: timedelta) -> None:
        for series, producer in self.outgoing.items():
            if producer.is_stale(outgoing_delta) and producer.done:
                del self.outgoing[series]
        for series, consumer in self.incoming.items():
            if consumer.is_stale(incoming_delta) and consumer.done:
                del self.incoming[series]
