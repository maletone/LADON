import unittest, random
from typing import Optional

from ladon.shared.interchange_pb2 import RemoteShell, LadonMessage
from ladon.shared.mailbox import Mailbox


class TestMailbox(unittest.TestCase):

    def test_e2e(self):
        message = LadonMessage()
        message.remote_shell.id = b'fooooooooooooooooooooooooooooooooooooooooooo'
        message.remote_shell.command = 'baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaar'

        # Note, of course, that if we had an existing RemoteShell message and wanted
        # to cram it into a LadonMessage, we'd have to "copy" it into the composite field--we can't
        # do a normal assignment
        # message = LadonMessage()
        # message.remote_shell.CopyFrom(my_remote_shell_message)

        mailbox = Mailbox()
        mailbox.prepare_message_for_sending(message)

        fake_wire = []
        while True:
            atom = mailbox.get_next_to_send(20)
            if atom is not None:
                fake_wire.append(atom)
            else:
                break

        received_message: Optional[LadonMessage]
        for atom in fake_wire:
            received_message = mailbox.handle_part(atom.message_part)
        self.assertEqual(message, received_message)

    def test_e2e_adversarial(self):
        message = LadonMessage()
        message.remote_shell.id = b'fooooooooooooooooooooooooooooooooooooooooooo'
        message.remote_shell.command = 'baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaar'

        mailbox = Mailbox()
        mailbox.prepare_message_for_sending(message)

        # Drain initial parts
        fake_wire = []
        while True:
            atom = mailbox.get_next_to_send(20)
            if atom is not None:
                fake_wire.append(atom)
            else:
                break

        # Shuffle, lose one
        random.shuffle(fake_wire)
        fake_wire.pop()

        # Receive initial parts
        received_message: Optional[LadonMessage]
        for atom in fake_wire:
            received_message = mailbox.handle_part(atom.message_part)
        self.assertIsNone(received_message)

        # Queue and drain retries
        fake_wire.clear()
        mailbox.queue_necessary_retries()
        while True:
            atom = mailbox.get_next_to_send(20)
            if atom is not None:
                fake_wire.append(atom)
            else:
                break

        # Receive retries
        for atom in fake_wire:
            mailbox.handle_retry(atom.part_retry_request)

        # Drain queued parts
        fake_wire.clear()
        while True:
            atom = mailbox.get_next_to_send(20)
            if atom is not None:
                fake_wire.append(atom)
            else:
                break
        for atom in fake_wire:
            received_message = mailbox.handle_part(atom.message_part)
        self.assertEqual(message, received_message)
