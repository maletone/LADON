import asyncio
import logging
import socket
from timeit import default_timer as timer
from typing import TypeVar, List, Optional, Awaitable

from google.protobuf.internal.decoder import _DecodeVarint
from google.protobuf.internal.encoder import _VarintEncoder

T = TypeVar('T')


def get_or_none(lst: List[T], idx: int) -> Optional[T]:
    try:
        return lst[idx]
    except IndexError:
        return None


# Open a port, get the number, close it, return the number
def get_port(socket_family=socket.AF_INET, socket_type=socket.SOCK_STREAM) -> int:
    s = socket.socket(socket_family, socket_type)
    s.bind(("", 0))
    port = s.getsockname()[1]
    s.close()
    return port


def decode_varint(data: bytes) -> int:
    return _DecodeVarint(data, 0)[0]


def encode_varint(value: int) -> bytes:
    data = []
    _VarintEncoder()(data.append, value, False)
    return b''.join(data)


async def read_varint_encoded(reader: asyncio.StreamReader) -> bytes:
    data = b''
    while True:
        try:
            data += await reader.readexactly(1)
            size = decode_varint(data)
            break
        except IndexError:
            pass
    return await reader.readexactly(size)


async def write_varint_encoded(writer: asyncio.StreamWriter, data: bytes) -> None:
    writer.write(encode_varint(len(data)) + data)
    await writer.drain()


async def flag_lengthy_execution(log: logging.Logger,
                                 coro: Awaitable[T],
                                 flag: int,
                                 description: str = "unknown",
                                 timeout: Optional[int] = None) -> T:

    async def flagger():
        await asyncio.sleep(flag)
        log.warning("FLAGGED LENGTHY EXECUTION: {0} at {1}".format(flag, description))

    flagger_task = asyncio.create_task(flagger())
    try:
        if timeout is not None:
            ret = await asyncio.wait_for(coro, timeout)
        else:
            ret = await coro
    finally:
        flagger_task.cancel()
    return ret


async def time_coro(coro: Awaitable[T], description: Optional[str] = None) -> T:
    start = timer()
    result = await coro
    end = timer()
    print("{0} took about {1} seconds".format(description or "coro", end - start))
    return result
