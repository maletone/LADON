import asyncio
import collections
import itertools
import random
from pathlib import Path
from typing import Dict, Iterable, Optional, Tuple

from google.protobuf.empty_pb2 import Empty

from ladon.protocol.protocol import ServerProtocol, ProtocolIdentity
from ladon.server.interface import ServerInterface
from ladon.shared.AbstractController import AbstractController
from ladon.shared.interchange_pb2 import LadonAtom
from ladon.shared.mailbox import Mailbox

from ladon.shared.util import flag_lengthy_execution


class ServerController(AbstractController):
    # Protocols server is listening on, associated to a map of different clients on that protocol
    #   Example: Channel TcpProtocol has "1.2.3.4" mapping to client "foo"
    protocols: Dict[ServerProtocol[ProtocolIdentity], Dict[ProtocolIdentity, bytes]]

    # Each client identifier, associated to a map of that client's identity on that protocol
    #   Example: Client "foo" is on TcpProtocol is known as "1.2.3.4", on UdpProtocol as "2.4.6.8", etc
    clients: Dict[bytes, Dict[ServerProtocol[ProtocolIdentity], ProtocolIdentity]] = {}

    _readable_client_ids: Dict[bytes, int] = {}

    # During SYN, SYN-ACK, ACK the exchange's state is the hello_id. If we associate the hello id to the client,
    # we'll know during a received ACK if it is errant or a full handshake.
    #   Example: Handshake "ABC" ongoing with client "foo" known as "1.2.3.4" on TcpProtocol
    hello_to_client: Dict[bytes, Tuple[bytes, ProtocolIdentity, ServerProtocol[ProtocolIdentity]]] = {}

    # We need to know what messages are currently in transit between us and the client. Maybe we'll also want to keep
    # track of per-client statistics.
    mailboxes: Dict[bytes, Mailbox] = {}

    interfaces: Dict[bytes, ServerInterface] = {}

    def __init__(self, temp: Path, protocols: Iterable[ServerProtocol]):
        super(ServerController, self).__init__(temp)
        self.protocols = {p: {} for p in protocols}

    def client_id_to_readable(self, client_id: bytes) -> int:
        if client_id not in self._readable_client_ids:
            self._readable_client_ids[client_id] = len(self._readable_client_ids.keys()) + 1
        return self._readable_client_ids[client_id]

    def client_readable_to_id(self, client_readable: int) -> bytes:
        for client_id, r in self._readable_client_ids.items():
            if r == client_readable:
                return client_id

    async def send_atom(self,
                        to_client: bytes,
                        to_send: Optional[LadonAtom] = None,
                        prompted_by_protocol: Optional[ServerProtocol] = None,
                        send_on_protocol: Optional[ServerProtocol] = None) -> bool:
        self.log.trace(
            "send_atom called: to_client {0}, to_send {1}, prompted_by_protocol {2}, send_on_protocol {3}".format(
                to_client, to_send, prompted_by_protocol, send_on_protocol
            ))

        if prompted_by_protocol is not None and prompted_by_protocol.REQUIRES_RESPONSE:
            if send_on_protocol is not None and send_on_protocol is not prompted_by_protocol:
                self.log.error("send_atom told to send on {0} but prompt from {1} requires response".format(
                    send_on_protocol, prompted_by_protocol
                ))
            send_on_protocol = prompted_by_protocol

        if send_on_protocol is None:
            available_protocols = [
                (protocol, identity) for protocol, identity in self.clients[to_client].items() if
                protocol.can_send_to(identity)
            ]
            if len(available_protocols) > 0:
                # TODO: Selection algorithm
                send_on_protocol, client_protocol_identity = random.choice(available_protocols)
            else:
                self.log.warning("send_atom no available protocols")
                return False
        else:
            client_protocol_identity = self.clients[to_client][send_on_protocol]

        if send_on_protocol.can_send_to(client_protocol_identity) is False:
            self.log.fatal(
                "send_atom bad state, {0} was selected for sending but reported it couldn't send to {1}".format(
                    send_on_protocol, client_protocol_identity
                ))
            raise RuntimeError

        if to_send is None:
            to_send = self.mailboxes[to_client].get_next_to_send(
                send_on_protocol.calculate_sendable_bytes(client_protocol_identity)
            )

        if to_send is None and prompted_by_protocol is not None and prompted_by_protocol.REQUIRES_RESPONSE:
            self.log.trace("send_atom forced to make empty server response")
            to_send = LadonAtom()
            to_send.server_ping_empty.CopyFrom(Empty())
        if to_send is not None:
            self.log.trace("send_atom handing off to {0}".format(send_on_protocol))
            await flag_lengthy_execution(self.log, send_on_protocol.send(client_protocol_identity, to_send),
                                         flag=5, description="server send_on_protocol")
            self.log.trace("send_atom got control back from {0}".format(send_on_protocol))
            return True
        else:
            self.log.warning("send_atom no effect; returning")
            return False

    async def handle_atom(self,
                          client_protocol_identity: ProtocolIdentity,
                          protocol: ServerProtocol[ProtocolIdentity],
                          atom: LadonAtom) -> None:
        atom_type = atom.WhichOneof("message_")
        atom_id = self.recv_count
        self.recv_count += 1

        stored_client = self.protocols[protocol].get(client_protocol_identity)

        self.log.info("Handling {0} {1} from {2} on {3}".format(
            atom_type,
            atom_id,
            stored_client or 'UNKNOWN',
            protocol
        ))

        response_atom: Optional[LadonAtom] = None

        # If SYN, note handshake and respond
        if atom_type == "client_syn":
            self.log.trace("Received client_syn")
            if stored_client is None:
                self.hello_to_client[atom.client_syn.hello_id] = (
                    atom.client_syn.client_id, client_protocol_identity, protocol)
                response_atom = LadonAtom()
                response_atom.server_syn_ack.hello_id = atom.client_syn.hello_id
                # If we're in handshake, we need to directly respond along this protocol. Not only do
                # we actually know everything needed to send right now, this protocol's info won't have been
                # recorded in the server's state--meaning that a call to send_atom wouldn't work.
                #
                # So, we just grab the protocol and tell it to do what we want and exit.
                await protocol.send(client_protocol_identity, response_atom)
                return
            else:
                raise RuntimeError

        # If SYN-ACK, someone screwed up, we aren't supposed to ever receive those server-side
        elif atom_type == "server_syn_ack":
            self.log.error("server_syn_ack received - This should not happen!")
            raise RuntimeError

        # If ACK, handshake complete, add client info and remove handshake note
        elif atom_type == "client_ack":
            handshake_record = self.hello_to_client.get(atom.client_ack.hello_id)
            if handshake_record is None:
                self.log.error("Received a client_ack but no record of the handshake existed")
                self.log.debug("client_ack reports a handshake of {0}".format(atom.client_ack.hello_id))
                self.log.debug("Handshake records include {0}".format(", ".join(map(str, self.hello_to_client.keys()))))
            else:
                handshake_client, handshake_client_identity, handshake_protocol = handshake_record
                if handshake_protocol != protocol:
                    self.log.debug("client_ack from {0} for {1} came via {2}".format(
                        handshake_client, handshake_protocol, protocol
                    ))
                else:
                    self.log.debug("client_ack from {0} for {1} came via itself".format(
                        handshake_client, handshake_protocol
                    ))

                if stored_client is None:
                    stored_client = handshake_client
                elif stored_client != handshake_client:
                    self.log.error("Handshake for client {0} was actually identified for existing client {1}".format(
                        handshake_client, stored_client
                    ))
                    self.log.trace("This is presumably one client that now occurs multiple times in server records")

                self.protocols[handshake_protocol][handshake_client_identity] = handshake_client
                if handshake_client not in self.clients:
                    self.clients[handshake_client] = {}
                self.clients[handshake_client][handshake_protocol] = handshake_client_identity
                del self.hello_to_client[atom.client_ack.hello_id]

                if handshake_client not in self.mailboxes:
                    self.mailboxes[handshake_client] = Mailbox()
                if handshake_client not in self.interfaces:
                    self.interfaces[handshake_client] = ServerInterface(
                        temp=self.temp / 'clients' / str(self.client_id_to_readable(handshake_client)),
                        prompt_sender=lambda msg: self.mailboxes[handshake_client].prepare_message_for_sending(msg)
                    )

        # If a part, have the record consume it and then do something with the possible message
        elif atom_type == "message_part":
            self.log.trace("message_part_received")
            res = self.mailboxes[stored_client].handle_part(atom.message_part)  # -> Optional[LadonMessage]
            if res is not None:
                self.log.info("Handling message {0} from {1}".format(
                    res.WhichOneof("message_"),
                    stored_client
                ))
                self.interfaces[stored_client].handle_response(res)

        # If a retry request, grab the part from the outgoing section of the client record and schedule sending
        elif atom_type == "part_retry_request":
            self.log.trace("part_retry_request received")
            self.mailboxes[stored_client].handle_retry(atom.part_retry_request)

        # If ping, prompt sending
        elif atom_type == "client_ping":
            self.log.trace("client_ping received")
            #   Why don't we do anything here?
            #       1. We don't need to tell the protocol anything. If the protocol requires atom receipt before
            #          sending, it would have already taken note of receiving this atom before calling this function.
            #       2. We don't need to force sending. If the protocol has requires_response=True then the conditional
            #          send_atom at the end of this function will always fire and send_atom will figure it out from
            #          there.

        # If a server ping response, someone screwed up, we aren't supposed to ever receive those server-side
        elif atom_type == "server_ping_empty":
            self.log.fatal("server_ping_empty received - This should not happen!")
            raise RuntimeError

        if stored_client is not None and (response_atom is not None or protocol.REQUIRES_RESPONSE):
            await self.send_atom(to_client=stored_client, to_send=response_atom, prompted_by_protocol=protocol)

        self.log.debug("Finished handling {0} {1} from {2}".format(
            atom_type,
            atom_id,
            stored_client or 'UNKNOWN'
        ))

    async def get_tasks(self) -> Iterable[asyncio.Task]:
        return itertools.chain(
            map(lambda p: asyncio.create_task(
                p.bind(self.handle_atom), name=str(p)
            ), self.protocols.keys()),
        )
