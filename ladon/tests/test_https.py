import asyncio
import unittest

from ladon.channels.simple_https.tcp_https import TcpServerProtocol, TcpServerDriver, TcpClientProtocol, TcpClientDriver
from ladon.client.client import ClientController
from ladon.server.server import ServerController
from ladon.shared.util import get_port


class TestTcp(unittest.IsolatedAsyncioTestCase):
    async def test_e2e(self):
        port = 443
        server_protocol = TcpServerProtocol(TcpServerDriver('127.0.0.1', port))
        client_protocol = TcpClientProtocol(TcpClientDriver('127.0.0.1', port))

        server = ServerController([server_protocol])
        client = ClientController([client_protocol])

        server_task = asyncio.create_task(server.start(), name=server.__class__.__name__)
        client_task = asyncio.create_task(client.start(), name=client.__class__.__name__)

        async def test_body():
            print('Test starting')
            await asyncio.sleep(20)
            print('Test ending')

        await asyncio.wait(
            [server_task, client_task, asyncio.create_task(test_body(), name="test_body")],
            return_when=asyncio.FIRST_COMPLETED
        )
