import asyncio
import unittest

import ladon.channels.kline_simple_udp.simple_udp
import ladon.client.client

kline_udp_client = ladon.channels.kline_simple_udp.simple_udp.UDPClientObject()

ladon_client = ladon.client.client.ClientController([kline_udp_client])


class TestHarness(unittest.IsolatedAsyncioTestCase):
    async def run(self):
        client_task = asyncio.create_task(ladon_client.start())

        async def test_body():
            print('Test starting')
            await asyncio.sleep(20)
            print('Test ending')

        await asyncio.gather(
            client_task,
            test_body(),
            return_exceptions=True
        )


def main():
    test = TestHarness()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test.run())


main()
