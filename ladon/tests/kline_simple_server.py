import unittest
import logging
import ladon.channels.kline_simple_udp.simple_udp
import ladon.client
import ladon.protocol
import ladon.server.server
import ladon.shared
import asyncio
import ladon.channels.shared_state_simulator.shared_state_simulator

kline_udp_server = ladon.channels.kline_simple_udp.simple_udp.UDPServerObject()

ladon_server = ladon.server.server.ServerController([kline_udp_server])


class TestHarness(unittest.IsolatedAsyncioTestCase):
    async def run(self):
        server_task = asyncio.create_task(ladon_server.start())

        async def test_body():
            print('Test starting')
            await asyncio.sleep(200)
            print('Test ending')

        await asyncio.gather(
            server_task,
            test_body(),
            return_exceptions=True
        )


def main():
    test = TestHarness()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test.run())


main()
