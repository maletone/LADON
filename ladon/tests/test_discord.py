import unittest
import logging
import ladon.channels.discord.bot_stuff.discord_channel
from ladon.client.client import ClientController
import ladon.protocol
from ladon.server.server import ServerController
import ladon.shared
import asyncio



class TestHarness(unittest.IsolatedAsyncioTestCase):
    async def test_discord(self):
        discord_server = ladon.channels.discord.bot_stuff.discord_channel.DiscordServer("ODIwMDE3NjA4ODY1NjExODA3.YEvC6Q.tS33g4kwurRpTJz8iVc-FXd_UcM",
                                                                                        831569253861687379)
        discord_client = ladon.channels.discord.bot_stuff.discord_channel.DiscordClient("ODIwMDE3NjA4ODY1NjExODA3.YEvC6Q.tS33g4kwurRpTJz8iVc-FXd_UcM",
                                                                                        831569253861687379)

        server = ServerController(None, [discord_server])
        client = ClientController(None, [discord_client])

        server_task = asyncio.create_task(server.start(), name=server.__class__.__name__)
        client_task = asyncio.create_task(client.start(), name=client.__class__.__name__)

        async def test_body():
            print('test starting')
            await asyncio.sleep(200)
            print('test ending')

        await asyncio.wait(
            [server_task, client_task, asyncio.create_task(test_body(), name="test body")],
            return_when=asyncio.FIRST_COMPLETED
        )

def main():
    test =TestHarness()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test.test_discord())

main()
