import asyncio
import ssl
from typing import Tuple, Callable, Awaitable, Optional

from ladon.protocol.client_led_protocol import ClspDriver, ClcpDriver, \
    ClientLedServerProtocol, ClientLedClientProtocol
from ladon.protocol.protocol import ProtocolIdentity
from ladon.shared.logging_setup import get_logger
from ladon.shared.util import read_varint_encoded, write_varint_encoded, get_port, time_coro

log = get_logger('TcpProtocol')

# The stable parts of a TCP "address":
#  - The IP as a `str`
# Yes, a tuple isn't strictly necessary here, but I figure standardization is good
TcpIdentity = Tuple[str]


# The "meat" of the server, this class just binds with a callback.
# Expects varint-length-prefixed incoming data and responds with the same.
class TcpServerDriver(ClspDriver[TcpIdentity]):
    ip: str = None
    port: int = None
    ssl_context: Optional[ssl.SSLContext] = None

    def __init__(self,
                 ip: Optional[str] = None,
                 port: Optional[int] = None,
                 ssl_context: Optional[ssl.SSLContext] = None):
        self.ip = ip or '0.0.0.0'
        self.port = port or get_port()
        if ssl_context is None:
            log.trace("{0} given no SSLContext, encryption disabled".format(self.__class__.__name__))
        else:
            self.ssl_context = ssl_context

    # This method is effectively top-level. This gets called once by the ServerController and it is expected
    # to run forever.
    async def bind(self, callback: Callable[[TcpIdentity,
                                             bytes,
                                             Callable[[bytes], Awaitable[None]]], Awaitable[None]]) -> None:
        # Function called exactly once per incoming connection. Remember, we will need to respond, but we don't do
        # that ourselves, the ServerController will. We just make some little function like `respond` below and pass
        # that up to the ServerController when we use `callback`.
        async def handle_incoming(reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
            # Get TcpIdentity (comma forces tuple of one element and is not a typo)
            tcp_identity = (writer.get_extra_info('peername')[0],)

            # Get data
            data = await read_varint_encoded(reader)

            # Make respond function
            async def respond(response_data: bytes) -> None:
                await write_varint_encoded(writer, response_data)
                writer.close()

            # Do the callback -- AKA we're set up here, pass what we've got up the chain
            await callback(tcp_identity, data, respond)

        # The callback ordering:
        #   1. `start_server` calls `handle_incoming` with a reader and writer
        #   2. `handle_incoming` calls `callback` when it has read incoming identity and data
        #   3. `callback` is guaranteed to eventually call `respond` to respond with data and close connection
        server = await asyncio.start_server(handle_incoming, host=self.ip, port=self.port, ssl=self.ssl_context)

        log.info("TcpServerDriver bound on {0}{1}".format(
            server.sockets[0].getsockname(), " + SSL" if self.ssl_context is not None else ""
        ))

        async with server:
            await server.serve_forever()


# Effectively a dummy class to help with naming and provide a way to override stuff in the protocol.
# See test_https.py for an example of instantiating this class.
class TcpServerProtocol(ClientLedServerProtocol[TcpIdentity]):
    # Technically the client_identity is ClientLedProtocolIdentity[TcpIdentity] but it's covariant here and that's long.
    def calculate_sendable_bytes(self, client_identity: ProtocolIdentity) -> Optional[int]:
        # TODO: For Tcp we could potentially get away with not setting a maximum.
        #   I'm doing it here for demo purposes.
        return 1450

    def __str__(self) -> str:
        if isinstance(self.driver, TcpServerDriver):
            return "{0} on {1}".format(super().__str__(), self.driver.port)
        else:
            return super().__str__()


# The "meat" of the server, this class just needs to be able to do one-off send/receives.
# Expects varint-length-prefixed incoming data and responds with the same.
class TcpClientDriver(ClcpDriver):
    server_ip: str
    server_port: int
    ssl_context: Optional[ssl.SSLContext] = None

    def __init__(self,
                 server_ip: str,
                 server_port: int,
                 ssl_context: Optional[ssl.SSLContext] = None):
        self.server_ip = server_ip
        self.server_port = server_port
        if ssl_context is None:
            log.trace("{0} given no SSLContext, encryption disabled".format(self.__class__.__name__))
        else:
            self.ssl_context = ssl_context

    # Just sends and expects back binary data.
    async def send_and_receive(self, data: bytes) -> bytes:
        reader, writer = await asyncio.open_connection(host=self.server_ip, port=self.server_port, ssl=self.ssl_context)
        await write_varint_encoded(writer, data)
        response = await read_varint_encoded(reader)
        writer.close()
        return response


# Effectively a dummy class to help with naming and provide a way to override stuff in the protocol.
# See test_https.py for an example of instantiating this class.
class TcpClientProtocol(ClientLedClientProtocol[TcpIdentity]):
    def calculate_sendable_bytes(self) -> Optional[int]:
        return 1450

    def __str__(self) -> str:
        if isinstance(self.driver, TcpClientDriver):
            return "{0} to {1}".format(super().__str__(), self.driver.server_port)
        else:
            return super().__str__()
