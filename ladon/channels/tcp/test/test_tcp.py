import asyncio
import os
import tempfile
import unittest
from pathlib import Path

from ladon.channels.tcp.tcp import TcpServerProtocol, TcpClientProtocol, TcpServerDriver, TcpClientDriver
from ladon.client.client import ClientController
from ladon.server.interface import ExfilHandler
from ladon.server.server import ServerController
from ladon.shared.interchange_pb2 import LadonMessage
from ladon.shared.util import get_port


class TestTcp(unittest.IsolatedAsyncioTestCase):
    async def test_no_ssl(self):
        with tempfile.TemporaryDirectory() as tempdirname:
            temp = Path(tempdirname)
            port = get_port()
            server_protocol = TcpServerProtocol(TcpServerDriver('127.0.0.1', port))
            client_protocol = TcpClientProtocol(TcpClientDriver('127.0.0.1', port))

            server = ServerController(temp, [server_protocol])
            client = ClientController(temp, [client_protocol])

            server_task = asyncio.create_task(server.start(), name=server.__class__.__name__)
            client_task = asyncio.create_task(client.start(), name=client.__class__.__name__)

            async def test_body():
                transfer_test_file = Path(__file__).parents[4] / 'FileUtils' / 'FileUtils' / 'ladon.py'
                assert transfer_test_file.exists(), "Test file doesn't exist"
                transfer_test_file_data = transfer_test_file.read_bytes()

                while len(server.interfaces) < 1:
                    await asyncio.sleep(1)

                assert len(server.interfaces) == 1, "Server has should have exactly one interface for one client"
                server_id_for_client: bytes = b''
                for interface_id in server.interfaces.keys():
                    server_id_for_client = interface_id
                assert len(server_id_for_client) != b'', "Couldn't identify client ID"
                assert server_id_for_client in server.mailboxes, "Client ID not in mailboxes"
                message = LadonMessage()
                message.exfil.path = str(transfer_test_file.absolute())
                message.exfil.id = os.urandom(4)
                outgoing_message_series = server.interfaces[server_id_for_client].handle_prompt(message)
                assert len(server.mailboxes[server_id_for_client].outgoing) == 1, "Server sending not 1 message"
                assert len(server.mailboxes[server_id_for_client].incoming) == 0, "Server receiving a message already"
                assert len(client.mailbox.outgoing) == 0, "Client sending a message already"
                assert len(client.mailbox.incoming) == 0, "Client receiving a message already"

                while len(server.mailboxes[server_id_for_client].incoming) < 1:
                    await asyncio.sleep(1)

                assert server.mailboxes[server_id_for_client].outgoing[outgoing_message_series].done, "Server not done sending yet"
                assert client.mailbox.incoming[outgoing_message_series].done, "Client not done receiving yet"
                assert len(client.mailbox.outgoing) == 1, "Client not sending 1 message"
                incoming_message_series = b''
                for series in client.mailbox.outgoing.keys():
                    incoming_message_series = series

                while not server.mailboxes[server_id_for_client].incoming[incoming_message_series].done:
                    await asyncio.sleep(1)

                # noinspection PyTypeChecker
                server_exfil_handler: ExfilHandler = server.interfaces[server_id_for_client].handlers["exfil"]
                assert len(server_exfil_handler.statuses) == 1, "Handling too many exfils"

                while server_exfil_handler.statuses[message.exfil.id][1] is None:
                    await asyncio.sleep(1)

                assert server_exfil_handler.statuses[message.exfil.id][1].read_bytes() == transfer_test_file_data, "Received file mismatch"

            await asyncio.wait(
                [server_task, client_task, asyncio.create_task(test_body(), name="test_body")],
                return_when=asyncio.FIRST_COMPLETED
            )
