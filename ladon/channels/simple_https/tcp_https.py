import asyncio
import ssl
from typing import Tuple, Callable, Awaitable, Optional
from cryptography.fernet import Fernet
from ladon.protocol.client_led_protocol import ClspDriver, ClcpDriver, \
    ClientLedServerProtocol, ClientLedClientProtocol
from ladon.protocol.protocol import ProtocolIdentity
from ladon.shared.logging_setup import get_logger
from ladon.shared.util import read_varint_encoded, write_varint_encoded
key = Fernet.generate_key()
f = Fernet(key)
#KEY = b'5be7b7B0Be9N8TR03hcrgMwekOTzrcQJodTLKv5su2k='
#FERN = Fernet(KEY)
LOCAL_ADDRESS = "0.0.0.0"
SERVER_IP_ADDRESS = "192.168.50.190"
SERVER_PORT = 443
CLIENT_PORT = 443

log = get_logger('TcpProtocol')

# The stable parts of a TCP "address":
#  - The IP as a `str`
# Yes, a tuple isn't strictly necessary here, but I figure standardization is good
TcpIdentity = Tuple[str]


# The "meat" of the server, this class just binds with a callback.
# Expects varint-length-prefixed incoming data and responds with the same.
class TcpServerDriver(ClspDriver[TcpIdentity]):
    ip: Optional[str] = None
    port: Optional[int] = None

    def __init__(self, ip: Optional[str] = None, port: Optional[int] = None):
        self.ip = ip
        self.port = port
        self.server_context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_2)
        self.server_context.check_hostname = False
        self.server_context.verify_mode = ssl.CERT_NONE
        self.server_context.set_ciphers("ADH-AES256-GCM-SHA384@SECLEVEL=0")
        self.server_context.load_dh_params("ladon/channels/simple_https/ssl/dhparams.pem")

    # This method is effectively top-level. This gets called once by the ServerController and it is expected
    # to run forever.
    async def bind(self, callback: Callable[[TcpIdentity,
                                             bytes,
                                             Callable[[bytes], Awaitable[None]]], Awaitable[None]]) -> None:
        # Function called exactly once per incoming connection. Remember, we will need to respond, but we don't do
        # that ourselves, the ServerController will. We just make some little function like `respond` below and pass
        # that up to the ServerController when we use `callback`.
        async def handle_incoming(reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
            # Get TcpIdentity
            tcp_identity = tuple(writer.get_extra_info('peername')[0])
            log.trace("TcpServerDriver handling incoming from {0}".format(tcp_identity))

            # Get data
            data = await read_varint_encoded(reader)

            # Make respond function
            async def respond(response_data: bytes) -> None:
                await write_varint_encoded(writer, response_data)
                writer.close()

            # Do the callback -- AKA we're set up here, pass what we've got up the chain
            await callback(tcp_identity, data, respond)

        # The callback ordering:
        #   1. `start_server` calls `handle_incoming` with a reader and writer
        #   2. `handle_incoming` calls `callback` when it has read incoming identity and data
        #   3. `callback` is guaranteed to eventually call `respond` to respond with data and close connection
        server = await asyncio.start_server(handle_incoming, host=self.ip, port=self.port, ssl=self.server_context)

        log.info("TcpServerDriver bound on {0}".format(server.sockets[0].getsockname()))

        async with server:
            await server.serve_forever()


# Effectively a dummy class to help with naming and provide a way to override stuff in the protocol.
# See test_https.py for an example of instantiating this class.
class TcpServerProtocol(ClientLedServerProtocol[TcpIdentity]):
    # Technically the client_identity is ClientLedProtocolIdentity[TcpIdentity] but it's covariant here and that's long.
    def calculate_sendable_bytes(self, client_identity: ProtocolIdentity) -> Optional[int]:
        # TODO: For Tcp we could potentially get away with not setting a maximum.
        #   I'm doing it here for demo purposes.
        return 48000


# The "meat" of the server, this class just needs to be able to do one-off send/receives.
# Expects varint-length-prefixed incoming data and responds with the same.
class TcpClientDriver(ClcpDriver):
    server_ip: str
    server_port: int

    def __init__(self, server_ip: str, server_port: int):
        self.server_ip = server_ip
        self.server_port = server_port
        self.context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_2)
        self.context.check_hostname = False
        self.context.verify_mode = ssl.CERT_NONE
        self.context.set_ciphers("ADH-AES256-GCM-SHA384@SECLEVEL=0")
        self.context.load_dh_params("ladon/channels/simple_https/ssl/dhparams.pem")

    # Just sends and expects back binary data.
    async def send_and_receive(self, data: bytes) -> bytes:

        reader, writer = await asyncio.open_connection(host=self.server_ip, port=self.server_port, ssl=self.context)
        await write_varint_encoded(writer, data)
        response = await read_varint_encoded(reader)
        writer.close()

        return response


# Effectively a dummy class to help with naming and provide a way to override stuff in the protocol.
# See test_https.py for an example of instantiating this class.
class TcpClientProtocol(ClientLedClientProtocol[TcpIdentity]):
    def calculate_sendable_bytes(self) -> Optional[int]:
        return 48000
