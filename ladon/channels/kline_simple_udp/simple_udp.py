import asyncio

from cryptography.fernet import Fernet

import ladon.protocol.protocol
from ladon.protocol.protocol import *
from ladon.shared.interchange_pb2 import LadonAtom
from ladon.shared.logging_setup import get_logger
from ladon.shared.util import get_port

client_log = get_logger('simple_UDP_client')
server_log = get_logger('simple_UDP_server')

"""
A simple UDP channel that will later evolve into a DNS channel.
"""
# Adding stuff to prepare for encryption, this is your key, you encrypt and decrypt with f.
# key = Fernet.generate_key()
# f = Fernet(key)
f = Fernet(b'UEhKGq0ttJiR_hBCJtBxhU2nbNKlALGpi6oPfiry1n4=')

SERVER_IP_ADDRESS = "172.24.0.25"
# SERVER_IP_ADDRESS = "127.0.0.1"
SERVER_PORT = 62000


class EchoClientProtocol:

    def __init__(self, message, on_con_lost, callback, parent_protocol_object):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.callback = callback
        self.parent_protocol_object = parent_protocol_object

    def connection_made(self, transport):
        self.transport = transport
        self.transport.sendto(self.message)
        client_log.debug("Sending message")

    def datagram_received(self, data, addr):
        client_log.debug("Data received")
        returnatom = LadonAtom()
        data = f.decrypt(data)
        returnatom.ParseFromString(data)
        asyncio.ensure_future(self.run_callback(addr, returnatom), loop=asyncio.get_event_loop())
        self.transport.close()
        client_log.debug("Transport closed(?)")

    def error_received(self, exc):
        client_log.error("Error received: {0}".format(exc))

    def connection_lost(self, exc):
        client_log.debug("Connection has closed")
        self.on_con_lost.set_result(True)

    async def run_callback(self, addr, returnatom):
        client_log.debug("Running callback")
        await self.parent_protocol_object.callback(returnatom, self.parent_protocol_object)
        client_log.debug("Callback has run")


class UDPClientObject(ladon.protocol.protocol.ClientProtocol):

    def __init__(self, server_ip: str, server_port: int):
        self.server_ip = server_ip
        self.server_port = server_port

    def __str__(self) -> str:
        return "{0} to {1}".format(
            super().__str__(), self.server_port
        )


    def calculate_sendable_bytes(self) -> int:
        return 48000

    async def send_syn(self, handshake_record: Dict[bytes, 'ClientProtocol'], client_id: bytes) -> Awaitable[None]:
        client_log.debug("send_syn called")
        syn = LadonAtom()
        syn.client_syn.client_id = self.client_ID
        syn.client_syn.hello_id = self.hello_ID
        handshake_record[syn.client_syn.hello_id] = self
        await self.send(syn)
        client_log.debug("Returning from send_syn")
        return

    async def bind(self, callback: Callable[[LadonAtom], Awaitable[None]],
                   handshake_record: Dict[bytes, 'ClientProtocol'], client_id: bytes) -> Awaitable[None]:
        client_log.debug("Bind running")
        self.callback = callback
        self.client_ID = client_id
        self.hello_ID = os.urandom(4)
        client_log.debug('Client_ID generated: {0}'.format(self.client_ID))
        client_log.debug('Hello_ID generated: {0}'.format(self.hello_ID))

        await self.send_syn(handshake_record, self.client_ID)

        while True:
            await asyncio.sleep(4)
            server_log.debug("Still running in bind loop")

    async def send(self, atom: LadonAtom) -> Awaitable[None]:
        client_log.debug("Send called")
        atom = atom.SerializeToString()
        atom = f.encrypt(atom)
        atom = self.client_ID + atom
        loop = asyncio.get_running_loop()
        on_con_lost = loop.create_future()
        transport, protocol = await loop.create_datagram_endpoint(
            lambda: EchoClientProtocol(atom, on_con_lost, self.callback, self),
            remote_addr=(self.server_ip, self.server_port))
        self.thing = protocol

        return


class EchoServerProtocol:
    callback = None
    parent_protocol_object = None

    def pass_callback_data(self, callback, parent_protocol_object):
        self.callback = callback
        self.parent_protocol_object = parent_protocol_object
        server_log.debug("Callback data assigned to EchoServerProtocol")

    def connection_made(self, transport):
        self.transport = transport
        server_log.debug("Connection made has run.  Assigned transport is {0}".format(transport))
        server_log.debug("self.transport is {0}".format(self.transport))

    def datagram_received(self, data, addr):
        server_log.debug("Datagram received running")
        server_log.debug("Received data: {0}".format(data))
        returnatom = LadonAtom()
        # TODO: THIS LOOKS WRONG - There's an overlap in the two lines below
        #   Why isn't this breaking stuff?  Must investigate to make sure it isn't causing problems
        client_id = data[0:2]
        data = f.decrypt(data[2:])
        returnatom.ParseFromString(data)

        self.parent_protocol_object.jank_NAT_table[client_id] = addr[1]

        try:
            asyncio.ensure_future(self.run_callback((addr[0], client_id), returnatom), loop=asyncio.get_event_loop())
        except:
            server_log.error("FAILURE")
        server_log.debug("Datagram received finished")

    async def run_callback(self, target, returnatom):
        server_log.debug("Preparing to await callback from EchoServerProtocol")
        await self.callback(target, self.parent_protocol_object, returnatom)
        server_log.debug("Awaiting of callback from EchoServerProtocol completed")


class UDPServerObject(ladon.protocol.protocol.ServerProtocol):
    REQUIRES_RESPONSE = True  # TODO: FOR DEBUG PURPOSES

    def __init__(self, ip: Optional[str] = None, port: Optional[int] = None):
        self.ip = ip or '0.0.0.0'
        self.port = port or get_port()

    def __str__(self) -> str:
        return "{0} on {1}".format(
            super().__str__(), self.port
        )

    """
    Ok, so this is... jank.
    So, we need to keep track of client IDs.  We can't do that with IP:Port, because ports change due to NAT.
    So we keep track of client IDs with IP and ID.
    But we need to keep track of the NAT port.  And we can't do that with client ID because it will change.
    And then we end up with different clients on the server corresponding to one real client.
    So instead, this server protocol object will have its own dictionary of the last time it got a message from an ID.
    It will then enter the most up-to-date NAT port on receiving a message.
    When this object is given an IP:ID pair (or whatever it is), it will look up the NAT port and send it there.
    This SUCKS.
    """
    jank_NAT_table = {}

    def calculate_sendable_bytes(self, client_identity: ProtocolIdentity) -> int:
        return 48000

    async def bind(self, callback: Callable[
        [ProtocolIdentity, 'ServerProtocol[ProtocolIdentity]', LadonAtom], Awaitable[None]]) -> Awaitable[None]:
        server_log.debug("Bind called")
        loop = asyncio.get_running_loop()
        transport, protocol = await loop.create_datagram_endpoint(EchoServerProtocol,
                                                                  local_addr=(self.ip, self.port))
        try:
            server_log.info("UDPServerObject bound on {0}".format(
                transport.get_extra_info('sockname')
            ))
        except:
            server_log.info("Could not read sockname for UDPServerObject, presumably bound on {0}:{1}".format(
                self.ip, self.port
            ))
        self.thing = protocol
        protocol.pass_callback_data(callback=callback, parent_protocol_object=self)

        while True:
            await asyncio.sleep(4)

    async def send(self, client_identity: ProtocolIdentity, atom: LadonAtom) -> Awaitable[None]:
        server_log.debug("Send called")
        server_log.debug("client_identity : {0} ; atom {1}".format(client_identity, atom))
        atom = atom.SerializeToString()
        atom = f.encrypt(atom)

        # NOTE - client_identity is a lie - this is actually (IP, Client_ID)
        # It then gets used to lookup the correct NAT port

        target = (client_identity[0], self.jank_NAT_table[client_identity[1]])

        try:
            self.thing.transport.sendto(atom, target)
        except:
            server_log.error("Send failed")
        server_log.debug("Send operation finished")
