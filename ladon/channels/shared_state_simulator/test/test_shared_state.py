import asyncio
import unittest

from ladon.channels.shared_state_simulator.shared_state_simulator import SharedState, SharedStateServerProtocol, \
    SharedStateClientProtocol
from ladon.client.client import ClientController
from ladon.server.server import ServerController


class TestSharedState(unittest.IsolatedAsyncioTestCase):
    async def test_e2e(self):
        # Shared state protocol setup
        shared_state = SharedState()
        server_protocol = SharedStateServerProtocol(shared_state)
        client_protocol = SharedStateClientProtocol(shared_state, server_protocol.server_identity)

        server = ServerController([server_protocol])
        client = ClientController([client_protocol])

        server_task = asyncio.create_task(server.start(), name=server.__class__.__name__)
        client_task = asyncio.create_task(client.start(), name=client.__class__.__name__)

        async def test_body():
            print('Test starting')
            await asyncio.sleep(20)
            print('Test ending')

        await asyncio.wait(
            [server_task, client_task, asyncio.create_task(test_body(), name="test_body")],
            return_when=asyncio.FIRST_COMPLETED
        )