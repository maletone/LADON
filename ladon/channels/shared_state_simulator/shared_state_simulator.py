import os
from asyncio import Queue
from typing import Tuple, Dict, Awaitable, Callable

from ladon.protocol.protocol import ServerProtocol, ClientProtocol
from ladon.shared.interchange_pb2 import LadonAtom

# Since this is a dummy protocol that doesn't actually "send" anything, we make up
# a sort of identity to build our protocols off of
SharedStateIdentity = bytes


def make_shared_state_identity() -> SharedStateIdentity:
    return os.urandom(4)


class SharedState:
    # Suppose there's a single incoming pipe to the server. Atoms get added associated to the client's identity.
    server_pipe: Queue[Tuple[SharedStateIdentity, LadonAtom]]

    # Suppose each client has a single incoming pipe. Atoms get added just to whatever pipe goes to that client.
    client_pipes: Dict[SharedStateIdentity, Queue[LadonAtom]] = {}


class SharedStateServerProtocol(ServerProtocol[SharedStateIdentity]):
    shared_state: SharedState
    get_atom_size: Callable[[], int]
    # We don't actually need to store this here--we don't need it server-side, and in fact the interface doesn't have it
    # However, we store it here just for simplicity's sake
    server_identity: SharedStateIdentity

    def __init__(self, shared_state: SharedState, get_atom_size=(lambda: 150)):
        self.shared_state = shared_state
        self.get_atom_size = get_atom_size
        self.server_identity = make_shared_state_identity()

    def calculate_sendable_bytes(self, client_identity: SharedStateIdentity) -> int:
        return self.get_atom_size()

    async def bind(self, callback: Callable[[SharedStateIdentity,
                                             'SharedStateServerProtocol',
                                             LadonAtom], Awaitable[None]]) -> None:
        # Server pipe init
        if not hasattr(self.shared_state, 'server_pipe'):
            self.shared_state.server_pipe = Queue()
        while True:
            identity, atom = await self.shared_state.server_pipe.get()
            await callback(identity, self, atom)

    async def send(self, client_identity: SharedStateIdentity, atom: LadonAtom) -> None:
        # Client pipe init
        if client_identity not in self.shared_state.client_pipes:
            self.shared_state.client_pipes[client_identity] = Queue()
        await self.shared_state.client_pipes[client_identity].put(atom)


class SharedStateClientProtocol(ClientProtocol[SharedStateIdentity]):
    shared_state: SharedState
    get_atom_size: Callable[[], int]
    # Like storing the server identity in the server, storing this here isn't strictly necessary--we normally don't
    # need it client-side. Here we use it because we have this weird shared state object, but in a normal
    # circumstance the server just learns and cares about it
    client_identity: SharedStateIdentity

    def __init__(self, shared_state: SharedState, server_identity: SharedStateIdentity, get_atom_size=(lambda: 150)):
        self.shared_state = shared_state
        self.server_identity = server_identity
        self.get_atom_size = get_atom_size
        self.client_identity = make_shared_state_identity()

    def calculate_sendable_bytes(self) -> int:
        return self.get_atom_size()

    async def bind(self,
                   callback: Callable[[LadonAtom,
                                       'SharedStateClientProtocol'], Awaitable[None]],
                   handshake_record: Dict[bytes, 'ClientProtocol'],
                   client_id: bytes) -> None:
        # Client pipe init
        if self.client_identity not in self.shared_state.client_pipes:
            self.shared_state.client_pipes[self.client_identity] = Queue()

        await self.send_syn(handshake_record, client_id)

        while True:
            atom = await self.shared_state.client_pipes[self.client_identity].get()
            await callback(atom, self)

    async def send(self, atom: LadonAtom) -> None:
        # Server pipe init
        if not hasattr(self.shared_state, 'server_pipe'):
            self.shared_state.server_pipe = Queue()
        await self.shared_state.server_pipe.put((self.client_identity, atom))
