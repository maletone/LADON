import asyncio
#from ladon.shared.logging_setup import get_logger
import socket

from google.protobuf import json_format

import ladon.protocol.protocol
from ladon.protocol.protocol import *
from ladon.shared.interchange_pb2 import LadonAtom

import discord
from discord.ext import commands
import json
import os
import requests
from dotenv import load_dotenv

import random
import string

log = get_logger(__file__)
log.propagate = False
'''
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
CHANNEL_ID = os.getenv("CHANNEL_ID")
'''
SharedStateIdentity = str

def make_shared_state_identity() -> SharedStateIdentity:
    letters = string.ascii_lowercase
    id = ''.join(random.choice(letters) for i in range(10))
    return id


class DiscordClient(ladon.protocol.protocol.ClientProtocol):
    token: int
    channel_id: int
    client_identity: SharedStateIdentity

    def __init__(self,
                #server_identity: str,
                token: str,
                channel_id:int):
        self.server_identity = None
        self.token = token
        self.channel_id = channel_id
        self.client_identity = make_shared_state_identity()



    def calculate_sendable_bytes(self) -> int:
        return 48000

    async def bind(self,
                   callback: Callable[[LadonAtom,
                                       'SharedStateClientProtocol'], Awaitable[None]],
                   handshake_record: Dict[bytes, 'ClientProtocol'],
                   client_id: str) -> None:
        #client.run(TOKEN)
        self.callback = callback
        self.client_ID = client_id
        client = discord.Client()
        self.client = client
        self.hello_id = os.urandom(4)
        @client.event
        async def on_ready():
            syn = LadonAtom()
            syn.client_syn.client_id = self.client_ID
            syn.client_syn.hello_id = self.hello_id
            handshake_record[syn.client_syn.hello_id] = self
            await self.send(syn)
        @client.event
        async def on_message(m):
            data = None
            if not self.server_identity and "," in m.content:
                self.server_identity = m.content[:11]
            if self.server_identity in m.content and self.client_identity in m.content:
                if len(m.attachments) > 0:
                    data = await m.attachments[0].read()
            returnatom = LadonAtom()
            if data:
                #data.decode('utf-8')
                returnatom.ParseFromString(data)
                await callback(returnatom, self)


        await asyncio.wait([client.start(self.token)])
        while True:
            await asyncio.sleep(1000000)

    async def send(self, atom: LadonAtom) -> None:
        await asyncio.sleep(3)
        channel = self.client.get_channel(self.channel_id)
        with open('test', 'wb') as f:
            a = atom.SerializeToString()
            f.write(a)
            f.close()
        with open('test', 'rb') as fp:
            await channel.send(self.client_identity, file=discord.File(fp, 'file'))
            fp.close()

class DiscordServer(ladon.protocol.protocol.ServerProtocol):
    token: int
    channel_id: int
    server_identity: SharedStateIdentity
    REQUIRES_RESPONSE = True
    def __init__(self,
                token: str,
                channel_id: int):
        self.token = token
        self.channel_id = channel_id
        self.server_identity = make_shared_state_identity()



    def calculate_sendable_bytes(self, client_identity: SharedStateIdentity) -> int:
        return 48000

    async def bind(self, callback: Callable[[SharedStateIdentity,
                                             'SharedStateServerProtocol',
                                             LadonAtom], Awaitable[None]]) -> None:
        client = discord.Client()
        self.client = client

        @client.event
        async def on_message(m):
            data = None
            id = None
            if not self.server_identity in m.content:
                id = m.content
                if len(m.attachments) > 0:
                    data = await m.attachments[0].read()
                #print(data)
            returnatom = LadonAtom()
            if data and id:
                #data.decode()
                returnatom.ParseFromString(data)
                #print('OMGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG')
                await callback(id, self, returnatom)

        await asyncio.wait([client.start(self.token)])

        while True:
            await asyncio.sleep(1000000)

    async def send(self, client_identity: SharedStateIdentity, atom: LadonAtom) -> None:
        #await asyncio.sleep(3)
        channel = self.client.get_channel(self.channel_id)
        with open('test', 'wb') as f:
            f.write(atom.SerializeToString())
            f.close()
        with open('test', 'rb') as fp:
            await channel.send(self.server_identity + ' , ' + client_identity, file=discord.File(fp, 'file'))
            fp.close()
