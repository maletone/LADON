import os
from dotenv import load_dotenv
import json
import asyncio
import aiohttp


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
CHANNEL_ID = os.getenv("CHANNEL_ID")

URL = "https://discordapp.com/api"


async def api_call(path, method="GET", **kwargs):
    """Return the JSON body of a call to Discord REST API."""
    defaults = {
        "headers": {
            "Authorization": f"Bot {TOKEN}",
            "User-Agent": "DiscordBot ("", 0.1)"
        }
    }
    kwargs = dict(defaults, **kwargs)
    async with aiohttp.ClientSession(loop=loop) as session:
        async with session.request(method, URL + path,
                                   **kwargs) as response:
         assert 200 == response.status, response.reason
         return await response.json()

async def heartbeat(ws, interval):
    """Send every interval ms the heatbeat message."""
    while True:
        await asyncio.sleep(interval / 1000)  # seconds
        await ws.send_json({
            "op": 1,  # Heartbeat
            "d": last_sequence
        })

async def start(url):
    async with aiohttp.ClientSession(loop=loop) as session:
        async with session.ws_connect(
                f"{url}?v=6&encoding=json") as ws:
            async for msg in ws:
                print(msg.type, msg.data)
                data = json.loads(msg.data)
                if data["op"] == 10:  # Hello
                    await ws.send_json({
                        "op": 2,  # Identify
                        "d": {
                            "token": TOKEN,
                            "properties": {},
                            "compress": False,
                            "large_threshold": 250
                            }
                        })
                elif data["op"] == 0:  # Dispatch
                    print(data['t'], data['d'])
                else:
                    print(data)
            async for msg in ws:
                data = json.loads(msg.data)
                if data["op"] == 10:  # Hello
                    asyncio.ensure_future(heartbeat(
                        ws,
                        data['d']['heartbeat_interval']))
                elif data["op"] == 11:  # Heartbeat ACK
                    pass


async def main():
    """Main program."""
    response = await api_call("/gateway")
    print(response)
    await start(response["url"])

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
