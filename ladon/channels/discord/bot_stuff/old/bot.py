# bot.py
import json
import os
import requests
from dotenv import load_dotenv
import discord
# 1
from discord.ext import commands
from discord import Webhook, RequestsWebhookAdapter
import requests

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
CHANNEL_ID = os.getenv("CHANNEL_ID")

baseURL = "https://discordapp.com/api/channels/{}/messages".format(CHANNEL_ID)


# 2
bot = commands.Bot(command_prefix='!')
headers = { "Authorization":"Bot {}".format(TOKEN),
            "User-Agent":"myBotThing (http://some.url, v0.1)",
            "Content-Type":"application/json", }
'''
message = input("Type something for the bot to say:")

POSTedJSON =  json.dumps ( {"content":message} )

r = requests.post(baseURL, headers = headers, data = POSTedJSON)
'''
@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')


@bot.command(name='ping')
async def ping(ctx):
    response = b'\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90'
    with open('test', 'wb') as writer:
        writer.write(response)
    with open("test", "rb") as fp:
        await ctx.send("here is this file: ", file=discord.File(fp, "testfile"))
    await ctx.send(response)


@bot.event
async def on_message(message):
    print("Message Content: {}".format(message.content))

bot.run(TOKEN)
