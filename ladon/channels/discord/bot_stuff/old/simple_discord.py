import asyncio
from ladon.shared.logging_setup import get_logger
import socket

from google.protobuf import json_format

import ladon.protocol.protocol
from ladon.protocol.protocol import *
from ladon.shared.interchange_pb2 import LadonAtom

import discord
from discord.ext import commands
import json
import os
import requests
from dotenv import load_dotenv

log = get_logger(__file__)
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
CHANNEL_ID = os.getenv("CHANNEL_ID")

class ExampleDiscordClient(ladon.protocol.protocol.ClientProtocol):


    def calculate_sendable_bytes(self) -> int:
        return 0


    async def send_syn(self, handshake_record: Dict[bytes, 'ClientProtocol'], client_id: bytes) -> Awaitable[None]:
        syn = LadonAtom()
        syn.client_syn.client_id = self.client_ID
        syn.client_syn.hello_id = self.hello_ID
        handshake_record[syn.client_syn.hello_id] = self

        await self.send(syn)
        return

    @bot.event
    async def on_ready():
        print(f'{bot.user.name} has connected to Discord as Client!')
'''
    @bot.event
    async def on_message(m):
        if m.content =="server":
            read_atom = LadonAtom()
            file_content = await m.attachments[0].read()
            read_atom.ParseFromString(file_content)
            print(read_atom)
'''
    async def bind(self, callback: Callable[[LadonAtom], Awaitable[None]],
                   handshake_record: Dict[bytes, 'ClientProtocol'], client_id: bytes) -> Awaitable[None]:
        #client.run(TOKEN)
        bot = discord.Client()
        self.callback = callback
        self.client_ID = b'DISCORDCLIENTID_' + os.urandom(4)
        self.hello_ID = b'DISCORDHELLOID_' + os.urandom(4)
'''
        loop = asyncio.get_event_loop()
        loop.run_until_complete(bot.start(TOKEN))
'''
        #want to gather start(), send() recv()
        await self.sendsyn(handshake_record, self.client_ID)

        def check(m):
                return m.content == 'server'
        while True:
            await asyncio.sleep(1)

            msg = await bot.wait_for('message', check=check)

            if msg:
                data = await m.attachments[0].read()
                returnatom = LadonAtom()
                returnatom.ParseFromString(data)
            else:
                print("NO MESSAGE, THIS FAILED")

            try:
                await callback(returnatom)
            except Exception as e:
                print(e)

    async def send(self, atom: LadonAtom) -> Awaitable[None]:
        channel = bot.get_channel(int(CHANNEL_ID))
        with open('test', 'wb') as f:
            f.write(atom.SerializeToString())
        with open("test", "rb") as fp:
            await channel.send("client", file=discord.File(fp, "testfile"))

        return
        '''
        js_atom = json_format.MessageToJson(atom)
        print js_atom
        await channel.send(js_atom)
        '''





class ExampleDiscordServer(ladon.protocol.protocol.ServerProtocol):

    global server
    server = discord.Client()

    def calculate_sendable_bytes(self, client_identity: ProtocolIdentity) -> int:
        return 0

    @bot.event
    async def on_ready():
        print(f'{bot.user.name} has connected to Discord as Server!')

    async def bind(self, callback: Callable[
        [ProtocolIdentity, 'ServerProtocol[ProtocolIdentity]', LadonAtom], Awaitable[None]]) -> Awaitable[None]:

        loop = asyncio.get_event_loop()
        loop.run_until_complete(server.start(TOKEN))

        def check(m):
            return m.content == 'client'
        while True:
            await asyncio.sleep(1)

            msg = await server.wait_for('message', check=check)

            if msg:
                data = await m.attachments[0].read()
                returnatom = LadonAtom()
                returnatom.ParseFromString(data)
            else:
                print("NO MESSAGE FROM CLIENT")
            try:
                await callback(returnatom)
            except Exception as e:
                print(e)

    async def send(self, client_identity: ProtocolIdentity, atom: LadonAtom) -> Awaitable[None]:
        channel = server.get_channel(int(CHANNEL_ID))
        with open('test', 'wb') as f:
            f.write(atom.SerializeToString())
        with open('test', 'rb') as fp:
            await channel.send("server", file=discord.File(fp, "testfile"))
        return
