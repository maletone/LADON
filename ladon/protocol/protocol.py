import os
from abc import ABC, abstractmethod
from typing import TypeVar, Generic, Callable, Awaitable, Dict, Optional
from ladon.shared.interchange_pb2 import LadonAtom
from ladon.shared.logging_setup import get_logger

log = get_logger('protocol_root')

# The protocol identity is best explained by example:
# Over UDP, when a packet comes in we know the IP/port and thus we know how to respond over that protocol.
# In other words, the protocol identity is the address and return address on the outside of the envelope.

ProtocolIdentity = TypeVar('ProtocolIdentity')


class AbstractProtocol(ABC):
    # Some protocols may require that server implementations always reply to clients or vice versa
    REQUIRES_RESPONSE = False

    def __str__(self):
        return self.__class__.__name__


class ServerProtocol(Generic[ProtocolIdentity], AbstractProtocol):
    # Some protocols have upper limits on the amount of bytes they can send at once.
    # They can override this method to expose that information
    def calculate_sendable_bytes(self, client_identity: ProtocolIdentity) -> Optional[int]:
        return None

    # Some stateful protocols cannot always send messages to arbitrary clients.
    # They can override this method to expose that information.
    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def can_send_to(self, client_identity: ProtocolIdentity) -> bool:
        return True

    @abstractmethod
    async def bind(self, callback: Callable[[ProtocolIdentity,
                                             'ServerProtocol[ProtocolIdentity]',
                                             LadonAtom], Awaitable[None]]) -> None:
        # Wait for clients to send atoms on this channel
        ...

    @abstractmethod
    async def send(self, client_identity: ProtocolIdentity, atom: LadonAtom) -> None:
        ...


class ClientProtocol(Generic[ProtocolIdentity], AbstractProtocol):
    # Some protocols have upper limits on the amount of bytes they can send at once.
    # They can override this method to expose that information
    def calculate_sendable_bytes(self) -> Optional[int]:
        return None

    # Some protocols cannot always send messages.
    # They can override this method to expose that information.
    # noinspection PyMethodMayBeStatic
    def can_send(self) -> bool:
        return True

    async def send_syn(self, handshake_record: Dict[bytes, 'ClientProtocol'], client_id: bytes) -> None:
        log.debug("Client initiating handshake on {0}".format(self))
        syn_atom = LadonAtom()
        syn_atom.client_syn.client_id = client_id
        syn_atom.client_syn.hello_id = os.urandom(2)
        handshake_record[syn_atom.client_syn.hello_id] = self
        await self.send(syn_atom)
        return

    # Runs forever
    @abstractmethod
    async def bind(self,
                   callback: Callable[[LadonAtom,
                                       'ClientProtocol[ProtocolIdentity]'], Awaitable[None]],
                   handshake_record: Dict[bytes, 'ClientProtocol'],
                   client_id: bytes) -> None:
        # Call send_syn, wait forever
        # The reason we don't call send_syn outside of bind is in case a protocol needs to somehow setup before send()
        # can be used
        ...

    @abstractmethod
    async def send(self, atom: LadonAtom) -> None:
        ...
