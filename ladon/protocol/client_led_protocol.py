import asyncio
import os
from abc import ABC, abstractmethod
from asyncio import Queue
from typing import Final, TypeVar, Tuple, Generic, Awaitable, Callable, Dict

from ladon.protocol.protocol import ClientProtocol, ServerProtocol
from ladon.shared.interchange_pb2 import LadonAtom
from ladon.shared.logging_setup import get_logger, TRACE_LEVEL

log = get_logger('client_led_protocol')

StableIdentityComponents = TypeVar('StableIdentityComponents', bound=tuple)


class ClientLedProtocolIdentity(Generic[StableIdentityComponents]):
    IDENTIFIER_LENGTH: Final = 2
    _identifier: bytes
    _stable_identity_components: StableIdentityComponents

    @staticmethod
    def make_identifier() -> bytes:
        return os.urandom(ClientLedProtocolIdentity.IDENTIFIER_LENGTH)

    def __init__(self, identifier: bytes, stable_identity_components: StableIdentityComponents):
        self._identifier = identifier
        self._stable_identity_components = stable_identity_components

    def __str__(self):
        return "{0} + {1}".format(self._identifier, self._stable_identity_components)

    def __key(self) -> Tuple[bytes, StableIdentityComponents]:
        return self._identifier, self._stable_identity_components

    def __eq__(self, other):
        return isinstance(other, ClientLedProtocolIdentity) and self.__key() == other.__key()

    def __hash__(self):
        return hash(self.__key())


# Generic over StableIdentityComponents because this class must parse them out of incoming connections
class ClspDriver(Generic[StableIdentityComponents], ABC):
    @abstractmethod
    async def bind(self, callback: Callable[[StableIdentityComponents,
                                             bytes,
                                             Callable[[bytes], Awaitable[None]]], Awaitable[None]]) -> None:
        # Runs forever, waiting for new connections from clients.
        # When one comes in:
        # 1. Parse out whatever StableIdentityComponents there are
        # 2. Call the callback, providing a function that when given bytes will
        #       a. send the bytes as the response
        #       b. shut down the connection
        ...


class ClientLedServerProtocol(ServerProtocol[ClientLedProtocolIdentity[StableIdentityComponents]],
                              Generic[StableIdentityComponents]):
    REQUIRES_RESPONSE = True

    # "Handles" of open connections from clients waiting for a response
    handles_to_use: Dict[ClientLedProtocolIdentity[StableIdentityComponents], Queue[Callable[[bytes], Awaitable[None]]]]

    # The actual implementation we'll use to wait for new connections over the network.
    # This is server, so the driver will run forever and we give it a callback to use
    # whenever a connection comes in from a client.
    driver: ClspDriver[StableIdentityComponents]

    def __init__(self, driver: ClspDriver):
        self.driver = driver
        self.handles_to_use = {}

    def can_send_to(self, client_identity: ClientLedProtocolIdentity[StableIdentityComponents]) -> bool:
        return client_identity in self.handles_to_use

    async def bind(self, callback: Callable[[ClientLedProtocolIdentity[StableIdentityComponents],
                                             'ClientLedServerProtocol[StableIdentityComponents]',
                                             LadonAtom], Awaitable[None]]) -> None:
        # A function that the driver will call whenever it gets a client connection
        async def handle(stable_identity_components: StableIdentityComponents,
                         data_from_client: bytes,
                         response_callable: Callable[[bytes], Awaitable[None]]):
            # Piece together the ClientLedProtocolIdentity object
            client_identity = ClientLedProtocolIdentity(
                data_from_client[:ClientLedProtocolIdentity.IDENTIFIER_LENGTH], stable_identity_components
            )

            log.trace("Server identified client_identity as {0}".format(client_identity))

            # Add the response_callable to our handles
            if client_identity not in self.handles_to_use:
                log.trace("Server has not received a message from client_identity {0} before".format(client_identity))
                self.handles_to_use[client_identity] = Queue()
                log.trace("Handle queue for {0} added, client_identity will be recognized in the future".format(
                    client_identity
                ))
            await self.handles_to_use[client_identity].put(response_callable)

            # Parse the atom and wait on the callback
            atom = LadonAtom()
            atom.ParseFromString(data_from_client[ClientLedProtocolIdentity.IDENTIFIER_LENGTH:])
            log.trace("Parsed {0} atom from {1}, passing upstream".format(
                atom.WhichOneof("message_"), client_identity
            ))
            await callback(client_identity, self, atom)

        # Give the driver our handler to call and wait
        await self.driver.bind(handle)

    async def send(self, client_identity: ClientLedProtocolIdentity[StableIdentityComponents], atom: LadonAtom) -> None:
        if client_identity not in self.handles_to_use:
            log.fatal("No handle queue exists for {0}, only for {1}".format(
                client_identity, ", ".join(map(str, self.handles_to_use.keys()))
            ))
            raise RuntimeError
        try:
            handle = await asyncio.wait_for(self.handles_to_use[client_identity].get(), timeout=1.0)
        except asyncio.TimeoutError as e:
            log.fatal("Told to send to {0} but timed out before a handle was available".format(client_identity))
            raise e
        await handle(atom.SerializeToString())


class ClcpDriver(ABC):
    @abstractmethod
    async def send_and_receive(self, data: bytes) -> bytes:
        # Sets up a connection, sends, receives, tears down.
        # Only tricky part is knowing how to contact the server, maybe hardcode or getenv or config file?
        ...


class ClientLedClientProtocol(ClientProtocol[ClientLedProtocolIdentity[StableIdentityComponents]],
                              Generic[StableIdentityComponents]):
    # Atoms ready to be "received" by `bind`
    atoms_to_receive: Queue[LadonAtom]

    # The identifier we prepend sent LadonAtoms with to solve the NAT discrimination problem.
    # https://discord.com/channels/802260136478638110/802260136478638114/823620023356096579
    identifier: bytes = ClientLedProtocolIdentity.make_identifier()

    # The actual implementation we'll use to send and receive bytes over the network.
    # This is the client, so the driver will init a connection, send data, receive some
    # data, and tear down the connection.
    driver: ClcpDriver

    def __init__(self, driver: ClcpDriver):
        self.driver = driver

    async def bind(self,
                   callback: Callable[[LadonAtom,
                                       'ClientLedClientProtocol[StableIdentityComponents]'], Awaitable[None]],
                   handshake_record: Dict[bytes, 'ClientProtocol'],
                   client_id: bytes) -> None:
        # Queue is from asyncio and will create an event loop (very bad!) if we create it outside of a context where
        # an event loop already exists (like an async function)
        self.atoms_to_receive = Queue()
        await asyncio.sleep(1)
        await self.send_syn(handshake_record, client_id)
        while True:
            atom = await self.atoms_to_receive.get()
            await callback(atom, self)

    async def send(self, atom: LadonAtom) -> None:
        log.trace("CLCP {0} sending {1}".format(self.__class__.__name__, atom))
        bytes_to_send: bytes = self.identifier + atom.SerializeToString()
        response: bytes = await self.driver.send_and_receive(bytes_to_send)
        response_atom = LadonAtom()
        response_atom.ParseFromString(response)
        log.trace("CLCP {0} got back {1}".format(self.__class__.__name__, response_atom))
        await self.atoms_to_receive.put(response_atom)
