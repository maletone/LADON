import argparse
from pathlib import Path
from typing import Iterable

from ladon.channels.kline_simple_udp.simple_udp import *
from ladon.channels.discord.bot_stuff import discord_channel
from ladon.channels.tcp.tcp import TcpClientProtocol, TcpClientDriver
from ladon.client.client import ClientController
from ladon.entrypoint.abstract_entrypoint import AbstractEntrypoint, Protocol, Controller
from ladon.protocol.protocol import ClientProtocol
from ladon.shared.ssl.ssl_util import get_no_verify_context
from ladon.shared.dns import hostname_str


class ClientEntrypoint(AbstractEntrypoint[ClientProtocol, ClientController]):
    def configure_protocol_subparsers(self, new_subcommand: Callable[[str], argparse.ArgumentParser]) -> None:
        tcp_parser = new_subcommand('tcp')
        tcp_parser.add_argument('server_ip', type=hostname_str)
        tcp_parser.add_argument('server_port', type=int)
        tcp_parser.add_argument('--no_verify_ssl', action='store_const', dest='ssl', const=get_no_verify_context())
        tcp_parser.set_defaults(ssl=None,
                                command=lambda n: TcpClientProtocol(TcpClientDriver(n.server_ip, n.server_port, n.ssl)))

        udp_parser = new_subcommand('udp')
        udp_parser.add_argument('server_ip', type=hostname_str)
        udp_parser.add_argument('server_port', type=int)
        udp_parser.set_defaults(command=lambda n: UDPClientObject(n.server_ip, n.server_port))

        discord_parser = new_subcommand('discord')
        discord_parser.add_argument('--token', type=str)
        discord_parser.add_argument('--channel_id', type=int)
        discord_parser.set_defaults(command = lambda n: discord_channel.DiscordClient(n.token, n.channel_id))


    def controller_factory(self, temp: Path, protocols: Iterable[Protocol],
                           namespace: argparse.Namespace) -> Controller:
        return ClientController(temp, protocols, namespace.heartbeat_interval)

    def configure_main_parser(self, subparser: argparse.ArgumentParser) -> None:
        super().configure_main_parser(subparser)
        subparser.add_argument('--heartbeat_interval', type=int, default=2)
