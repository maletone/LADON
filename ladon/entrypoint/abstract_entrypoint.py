import abc
import argparse
import asyncio
import tempfile
import textwrap
from pathlib import Path
from typing import TypeVar, Generic, Iterable, Callable

from ladon.protocol.protocol import AbstractProtocol
from ladon.shared.AbstractController import AbstractController

Protocol = TypeVar('Protocol', bound=AbstractProtocol)
Controller = TypeVar('Controller', bound=AbstractController)

_temporary_directory = tempfile.TemporaryDirectory()


class AbstractEntrypoint(Generic[Protocol, Controller], abc.ABC):

    @abc.abstractmethod
    def configure_protocol_subparsers(self, new_subcommand: Callable[[str], argparse.ArgumentParser]) -> None:
        ...

    def parse_protocols(self, string: str) -> Iterable[Protocol]:
        parser = argparse.ArgumentParser(
            prog='--protocols',
            add_help=False,
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog=textwrap.dedent('''\
            Pass a comma-separated string containing multiple protocols and options.
            Example:
                --protocols 'tcp --port 123, udp --ip 1.2.3.4'
                
            To see help for an individual protocol:
                --protocols 'tcp -h'
            ''')
        )
        subparsers = parser.add_subparsers(dest='protocol_or_help')
        subparsers.required = True
        self.configure_protocol_subparsers(lambda name: subparsers.add_parser(name))
        help_parser = subparsers.add_parser('help')
        help_parser.set_defaults(command=lambda _: parser.exit(message=parser.format_help()))

        def parse_protocol(protocol_string: str) -> Protocol:
            args = parser.parse_args(protocol_string.split())
            return args.command(args)

        return map(parse_protocol, string.split(','))

    @abc.abstractmethod
    def controller_factory(self, temp: Path, protocols: Iterable[Protocol], namespace: argparse.Namespace) -> Controller:
        ...

    async def secondary_task_factory(self,
                                     controller: Controller,
                                     namespace: argparse.Namespace) -> Iterable[asyncio.Task]:
        return []

    def entrypoint(self, namespace: argparse.Namespace):
        controller = self.controller_factory(namespace.temp, self.parse_protocols(namespace.protocols), namespace)

        async def fn():
            return await asyncio.gather(
                controller.start(),
                *(await self.secondary_task_factory(controller, namespace)),
                return_exceptions=False
            )

        asyncio.run(fn())

    def configure_main_parser(self, subparser: argparse.ArgumentParser) -> None:
        subparser.add_argument('--protocols', '-p', required=True, type=str,
                               help='string with comma-separated protocols, "--protocols help" for more')
        subparser.add_argument('--temp', '-t', required=False, type=Path,
                               help='temporary directory to use for scratch work',
                               default=Path(_temporary_directory.name))
        subparser.set_defaults(run=self.entrypoint)
