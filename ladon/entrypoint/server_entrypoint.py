import argparse
import itertools
from pathlib import Path
from typing import Iterable

from ladon.channels.kline_simple_udp.simple_udp import *
from ladon.channels.discord.bot_stuff import discord_channel
from ladon.channels.tcp.tcp import TcpServerProtocol, TcpServerDriver
from ladon.entrypoint.abstract_entrypoint import AbstractEntrypoint, Controller, Protocol
from ladon.protocol.protocol import ServerProtocol
from ladon.server import api
from ladon.server.server import ServerController
from ladon.shared.ssl.ssl_util import get_no_verify_context
from ladon.shared.dns import hostname_str


class ServerEntrypoint(AbstractEntrypoint[ServerProtocol, ServerController]):
    def configure_protocol_subparsers(self, new_subcommand: Callable[[str], argparse.ArgumentParser]) -> None:
        tcp_parser = new_subcommand('tcp')
        tcp_parser.add_argument('--ip', type=hostname_str, default=None)
        tcp_parser.add_argument('--port', type=int, default=None)
        tcp_parser.add_argument('--no_verify_ssl', action='store_const', dest='ssl', const=get_no_verify_context())
        tcp_parser.set_defaults(ssl=None, command=lambda n: TcpServerProtocol(TcpServerDriver(n.ip, n.port, n.ssl)))

        udp_parser = new_subcommand('udp')
        udp_parser.add_argument('--ip', type=hostname_str, default=None)
        udp_parser.add_argument('--port', type=int, default=None)
        udp_parser.set_defaults(command=lambda n: UDPServerObject(n.ip, n.port))

        discord_parser = new_subcommand('discord')
        discord_parser.add_argument('--token', type=str, default=None)
        discord_parser.add_argument('--channel_id', type=int, default=None)
        discord_parser.set_defaults(command=lambda n: discord_channel.DiscordServer(n.token, n.channel_id))

    async def secondary_task_factory(self,
                                     controller: Controller,
                                     namespace: argparse.Namespace) -> Iterable[asyncio.Task]:
        api_task, _ = await api.as_task(controller=controller, temp=namespace.temp, host=namespace.api_ip, port=namespace.api_port)
        return itertools.chain(
            await super().secondary_task_factory(controller, namespace),
            [api_task]
        )

    def controller_factory(self, temp: Path, protocols: Iterable[Protocol], namespace: argparse.Namespace) -> Controller:
        return ServerController(temp, protocols)

    def configure_main_parser(self, subparser: argparse.ArgumentParser) -> None:
        super().configure_main_parser(subparser)
        subparser.add_argument('--api_ip', type=hostname_str, default='127.0.0.1')
        subparser.add_argument('--api_port', type=int, default=8080)
