import argparse

from ladon.entrypoint.client_entrypoint import ClientEntrypoint
from ladon.entrypoint.server_entrypoint import ServerEntrypoint

main_parser = argparse.ArgumentParser(
    description='Adaptive multi-channel C2 system',
    prog='ladon.entrypoint'
)
main_subparsers = main_parser.add_subparsers(dest='client_or_server')
main_subparsers.required = True
ClientEntrypoint().configure_main_parser(main_subparsers.add_parser('client'))
ServerEntrypoint().configure_main_parser(main_subparsers.add_parser('server'))
main_args = main_parser.parse_args()
main_args.run(main_args)
