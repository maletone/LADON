import random
import argparse

parser = argparse.ArgumentParser(description="Generate fake data for exfil testing")
parser.add_argument('type', type=str, help='Type of data to write - Options are cards or users')
parser.add_argument('rows', type=int, help='Number of rows to write')

args = parser.parse_args()


def gen_cc():
    def number():
        return random.randint(0, 9)

    def cc():
        four_d = "{0}{1}{2}{3}".format(number(), number(), number(), number())
        return four_d

    def full_cc():
        four_d = "{0}-{1}-{2}-{3}".format(cc(), cc(), cc(), cc())
        return four_d

    def cvv():
        three_d = "{0}{1}{2}".format(number(), number(), number())
        return three_d

    def exp_date():
        ed = "{0}/{1}".format(str(random.randint(1, 12)).zfill(2), random.randint(22, 24))
        return ed

    card = "VISA,{0},{1},{2}\n".format(full_cc(), cvv(), exp_date())
    return card


def gen_user():
    pass


with open("exfil_data.csv", "w") as f:
    if args.type == "cards":
        f.write("FAKE,CREDIT,CARD,CSV,FOR,TESTING\n")
        for x in range(args.rows):
            f.write(gen_cc())
