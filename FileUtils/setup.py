import setuptools

setuptools.setup(
    name="FileUtils",
    packages=['FileUtils'],
    python_requires=">=3.6",
    include_package_data=True,
)
